﻿using System.Web.Mvc;
using log4net;

namespace SayYeahTV.Web.Filters
{
    public class ExceptionsLoggingAttribute : HandleErrorAttribute
    {
        #region  Fields

        private readonly ILog _logger;

        #endregion

        #region Constructors

        public ExceptionsLoggingAttribute(ILog logger)
        {
            _logger = logger;
        }

        #endregion

        #region Methods

        public override void OnException(ExceptionContext filterContext)
        {
            _logger.Error("Unhandled exception", filterContext.Exception);
        }

        #endregion
    }
}