/// <vs BeforeBuild='mainCSS, mainJS, mainFonts' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');

gulp.task('mainJS', function () {
    return gulp.src(mainBowerFiles('**/*.js'))
        .pipe(gulp.dest('Scripts'));
});

gulp.task('mainCSS', function () {
    return gulp.src(mainBowerFiles('**/*.css'))
        .pipe(gulp.dest('Content'));
});

gulp.task('mainFonts', function () {
    return gulp.src(mainBowerFiles(['**/*.woff', '**/*.woff2', '**/*.svg', '**/*.ttf']))
        .pipe(gulp.dest('Fonts'));
});