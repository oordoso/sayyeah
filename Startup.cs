﻿using System.Configuration;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SayYeahTV.Web.Startup))]
namespace SayYeahTV.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalHost.DependencyResolver.UseServiceBus(ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"], "SayYeahChat");
            app.MapSignalR();
        }
    }
}
