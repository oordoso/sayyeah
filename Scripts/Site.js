﻿var App = {};
var shareDataConfig = {};

App.timezoneOffset = -new Date().getTimezoneOffset() / 60;

App.IsAndroid = navigator.userAgent.toLowerCase().indexOf('android') > -1;

App.VideoPlayer = {
    Mute: function (id, flag) {
        if (!App.IsAndroid)
            jwplayer(id).setMute(flag);
    },
    Volume: function (id, percent) {
        if (!App.IsAndroid)
            jwplayer(id).setVolume(percent);
    },
    Set: function (id, params) {
        return new App.VideoPlayer.Init(id).Setup(params);
    },
    Refresh: function (id, muted) {
        console.log('App.VideoPlayer.Refresh: ' + id);
        if (!App.IsAndroid)
            setTimeout(function() { //this is fix for tabs. It`s not working without it
                jwplayer(id).stop().play().setVolume(muted ? 0 : 100);
            }, 100);

    },
    Init: function (id) {
        var that = this;

        this.id = id;
        //this.player;

        /*if (!App.IsAndroid)*/
            this.player = jwplayer(this.id);

        //this.container = $('#' + this.id);

        this.Setup = function (params) {
            params = App.VideoPlayer.GetJwplayerParams(params, this);
            //console.log(params);

/*            if (App.IsAndroid) {
                console.log("ANDROID VIDEO", params.sources[0].file);
                $("#" + this.id).empty().addClass('jwplayer').append(
                    $('<video/>', {
                        'width': params.width,
                        'height': params.height,
                        'src': params.sources[0].file,
                        'controls': params.controlbar,
                    })
                );
            }

            else {*/
                //alert(3);
                //console.log('this.player');
                //console.log(this.player);                
            this.player = this.player.setup(params);

            this.player.on('adComplete adSkipped', function () {
                console.log('adComplete');
                $('#' + that.id).siblings('.presenter-webshow-logo-overlay, .presenter-logo-overlay').show();
            });

            this.player.on('adPlay', function () {
                console.log('adPlay');
                $('#' + that.id).siblings('.presenter-webshow-logo-overlay, .presenter-logo-overlay').hide();
            });

            /*}*/
            return this;
        };
        this.Mute = function (flag) {
            if (!App.IsAndroid)
                this.player.setMute(flag);

            return this;
        };
        this.Volume = function (percent) {
            if (!App.IsAndroid)
                this.player.setVolume(percent);

            return this;
        };
        this.Play = function () {
            if (!App.IsAndroid)
                this.player.play(true);

            return this;
        };
        this.Stop = function () {
            if (!App.IsAndroid)
                this.player.stop();

            return this;
        };
        this.OnIdle = function (callback) {
            if (!App.IsAndroid)
                this.player.onIdle(callback);

            return this;
        };

        this.OnPlay = function (callback) {
            console.log('OnPlay');
            console.log(callback);
            console.log(this.player);
            if (!App.IsAndroid)
                this.player.onPlay(callback);

            return this;
        };

        this.GetPosition = function () {
            return /*App.IsAndroid ? 0 :*/ this.player.getPosition();
        };

        this.OnDisplayClick = function (callback) {
            if (!App.IsAndroid)
                this.player.onDisplayClick(callback);

            return this;
        };

        return this;
    },
    GetJwplayerParams: function (param, player) {
        if (param.file) {
            param.sources = App.VideoPlayer.ProcessVideoUrl(param.file, player);
            delete param.file;
        }
        
        return $.extend({}, {
            wmode: 'opaque',
            image: App.Settings.playerLogo,
            //controlbar: 'bottom',
            controls: true,
            width: '100%',
            aspectratio: '16:9',
            //height: '460',
            //primary: 'html5',
            volume: 100,
            logo: null, //{file: App.Settings.mobeyeIco},
            autostart: true,
            primary: 'flash',
            //http://support.jwplayer.com/customer/en/portal/articles/1413089-javascript-api-reference#advertising
            advertising: {
                client: 'googima',
                //tag: 'http://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator='
                schedule: {
                    preroll: {
                        offset: 'pre',
                        tag: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/89965927/SayYeah_OnlineConGaboRamos_Preroll&ciu_szs=300x250,728x90&cust_params=searchterm%3Daudi%26gender%3Dfemale%26presenter%3Dtrue%26userlevel%3Dnormal%26loggeduser%3Dfalse%26video%3Dlive%26category%3Dfashion%2Cfestival%2Cmusic%26episodeid%3D%26interests%3D%26age%3D15&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
                    },
                    midroll1: {
                        offset: '420',
                        tag: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/89965927/SayYeah_OnlineConGaboRamos_Midroll1&ciu_szs=300x250,728x90&cust_params=searchterm%3Daudi%26gender%3Dmale%26presenter%3Dfalse%26userlevel%3Dpresenter%26loggeduser%3Dfalse%26video%3Dlive%26category%3Dfashion%2Cfestival%2Csport%26episodeid%3D%26interests%3D%26age%3D15&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
                    },
                    midroll2: {
                        offset: '840',
                        tag: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/89965927/SayYeah_OnlineConGaboRamos_Midroll2&ciu_szs=300x250,728x90&cust_params=searchterm%3Daudi%26gender%3Dmale%26presenter%3Dfalse%26userlevel%3Dpresenter%26loggeduser%3Dfalse%26video%3Dlive%26category%3Dfashion%2Cfestival%2Csport%26episodeid%3D%26interests%3D%26age%3D15&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
                    },
                    midroll3: {
                        offset: '1500',
                        tag: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/89965927/SayYeah_OnlineConGaboRamos_Midroll3&ciu_szs=300x250,728x90&cust_params=searchterm%3Daudi%26gender%3Dmale%26presenter%3Dfalse%26userlevel%3Dpresenter%26loggeduser%3Dfalse%26video%3Dlive%26category%3Dfashion%2Cfestival%2Csport%26episodeid%3D%26interests%3D%26age%3D15&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
                    },
                    midroll4: {
                        offset: '2400',
                        tag: 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/89965927/SayYeah_OnlineConGaboRamos_Midroll4&ciu_szs=300x250,728x90&cust_params=searchterm%3Daudi%26gender%3Dmale%26presenter%3Dfalse%26userlevel%3Dpresenter%26loggeduser%3Dfalse%26video%3Dlive%26category%3Dfashion%2Cfestival%2Csport%26episodeid%3D%26interests%3D%26age%3D15&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]'
                    }
                }
            },
            analytics: {
                enabled: false,
                cookies: false
            },
            //ga: {}
            flashplayer: '/Content/jwplayer/jwplayer.flash.swf'
        }, param);
    },
    ProcessVideoUrl: function (url, player) {
        function muteIfCurrentUser(){
            var live = $('#modal-broadcaster object param[name=flashvars]');
            if (live.length == 0 || !url){
                return;
            }
            var param = live.attr('value');
            var id = param.match(/fileName=(.*?)\?/)
            var id2 = url.match(/mp4:([^_]*_[^?]*)?/)
            if (id && id2 && id[1] && id2[1] && id[1] == id2[1]){
                var handle = setInterval(function(){
                    try{
                        player.Mute(true)
                    }catch (e){
                        clearInterval(handle)
                    }
                },100)
            }
        }


        var sources = [];

        if (url) {
            // If viewing on Android, then using rtsp stream
/*            if (App.IsAndroid) {
                url = url.replace("rtmp://", "rtsp://");
                // If the broadcast from infodraw, then add _aac postfix.
                if (/infodraw_\d{1,10}\?bid/i.test(url)) {
                    url = url.replace("?bid=", "_aac?bid=");
                }
                sources.push({ file: url });
            } else if (url) {*/
                sources.push({ file: url });
                // TODO : check this url !!!!
                if (url.indexOf('http://') === -1)
                    sources.push({ file: url.replace('rtmp://', 'http://').replace('?', '/playlist.m3u8?') });
            /*}*/
        }

        muteIfCurrentUser();
        console.log('SOURCES', sources);

        return sources;
    }
};

/* Broadcaster  */
var Broadcaster = function () {
    this.defaultSettings = this.settings = {
        fileName: null,
        publishingUrl: null,
        target: null,
        lang: 'EN',
        isRecord: false,
        resolutions: '480x360,640x480,480x270,640x360',
        onBeforeStart: function (type) {
            this.Start(type);
        },
        onStart: function () {
            
        },
        onStop: function() {
            
        }
    };
}

Broadcaster.prototype.Init = function (settings) {
    console.log('Loading broadcaster');
    console.log(settings);

    this.settings = $.extend({}, this.defaultSettings, settings);

    var flashvars = {
        fileName: this.settings.fileName, //data.Name,
        srv: this.settings.publishingUrl,//data.PublishingUrl,
        lang: this.settings.lang,//'@Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToLower()',
        resolutions: this.settings.resolutions//'@AppSettings.WebCameraResolutions'
    };

    var params = {
        wmode: 'window'
    };

    var attributes = {
        id: 'myFlashContent',
        classid: 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000',
    };

    swfobject.embedSWF('/Content/broadcaster/VideoRecording.swf?v=1.0.8', this.settings.target, '410', '300', '18.0.0', false, flashvars, params, attributes);
}

Broadcaster.prototype.OnStart = function() {
    this.settings.onStart.call(this);
}

Broadcaster.prototype.OnStop = function() {
    this.settings.onStop.call(this);
}

Broadcaster.prototype.OnBeforeStart = function (type) {
    console.log('Broadcaster.prototype.OnBeforeStart');
    this.settings.onBeforeStart.call(this, type);
}

Broadcaster.prototype.Start = function (type) {
    //console.log('Broadcaster.prototype.Start: ' + type);
    var swf = swfobject.getObjectById('myFlashContent');
    swf.doBroadcast();

    //if (type == 'recording')
    if (this.settings.isRecord)
        swf.doRecord();

    this.OnStart();
}


//swfobject.registerObject("myFlashContent", "11.0.0");

//var swf = swfobject.getObjectById("myFlashContent");

App.Broadcaster = new Broadcaster();

function onBeforeStartingBroadcast(type) {
    console.log('onBeforeStartingBroadcast');
    App.Broadcaster.OnBeforeStart(type);
}

function onStopBroadcast() {
    App.Broadcaster.OnStop();
}

var PushServer = function () {

    this.OnConnectCallbacks = [];
    this.OnDisconnectCallbacks = [];
    this.Subscribers = {};
    //this.Url = url;
    this.handler = null;
    this.IsConnected = false;

    this.preparePacket = function(cmd, data) {
        var out = {
            cmd: cmd,
            data: data
        };
        return JSON.stringify(out);
    };

    this.onData = function(cmd, data) {
        var subscribers = this.Subscribers[cmd];
        if (subscribers && subscribers.length > 0) {
            for (var i in subscribers) {
                var callback = subscribers[i];
                callback.call(this, data);
            }
        }
    };
};

PushServer.Commands = {
    Init: 1,
    InitChat: 2,
    Messages: 3,
    UpdateChannelVideo: 4,
    UserConnected: 5,
    UserDisconnected: 6,
    IncrementNotificationsCounter: 7,
    AddView: 8,
    Vote: 9,
    UpdateCounters: 10,
    UpdateLiveBroadcasts: 11,
    GetChatters: 12
};

PushServer.prototype.start = function() {
    var that = this;
    
    var hub = this.handler = $.connection.chatHub;

    hub.client.sendMessage = function (message) {
        //console.log(message);
        that.onData(PushServer.Commands.Messages, [message]);
    };

    hub.client.sendMessages = function (messages) {
        //console.log(message);
        that.onData(PushServer.Commands.Messages, messages);
    };

    hub.client.updateChannelVideo = function (data) {
        console.log('hub.client.updateChannelVideo');
        console.log(data);
        that.onData(PushServer.Commands.UpdateChannelVideo, data);
    };

    hub.client.updateLiveBroadcasts = function () {
        that.onData(PushServer.Commands.UpdateLiveBroadcasts);
    };
    
    hub.client.updateCounters = function (counters) {
        //console.log(message);
        that.onData(PushServer.Commands.UpdateCounters, counters);
    };

    hub.client.userConnected = function (user) {
        that.onData(PushServer.Commands.UserConnected, user);
    };

    hub.client.usersConnected = function (users) {
        that.onData(PushServer.Commands.UserConnected, users);
    };

    hub.client.userDisconnected = function (user) {
        that.onData(PushServer.Commands.UserDisconnected, user.Id);
    };

    hub.client.incrementNotificationsCounter = function () {
        that.onData(PushServer.Commands.IncrementNotificationsCounter);
    };
    
    $.connection.hub.stateChanged(function (change) {
        console.log('new State' + change.newState);
        if (change.newState === $.signalR.connectionState.disconnected ||
            change.newState === $.signalR.connectionState.reconnecting) {
            
            that.IsConnected = false;
            for (var i in that.OnDisconnectCallbacks) {
                var callback = that.OnDisconnectCallbacks[i];
                callback.call(that);
            }
            //if (that.onState)
            //that.onState('disconnected');

            $.connection.hub.logging = true;

            $.connection.hub.start();
        }
        
        if (change.newState === $.signalR.connectionState.reconnected ||
            change.newState === $.signalR.connectionState.connected) {
            
            that.IsConnected = true;
            for (var i in that.OnConnectCallbacks) {
                var callback = that.OnConnectCallbacks[i];
                callback.call(that);
            }
            console.log('The server is online');
            //if (that.onState)
                //that.onState('connected');
        }
    });

    $.connection.hub.error(function (error) {
        console.log('error ' + error);
        if (that.onState)
            that.onState('error');
    });
    
    $.connection.hub.logging = true;

    $.connection.hub.reconnected(function () {
        console.log('Reconnected');
        //hub.server.increment(0);
    });

    $.connection.hub.start().done(function () {
        console.log('hub started');
    });

};

PushServer.prototype.send = function(cmd, data, isGuarantee) {
    //if(!this.Sock)
    //throw new Exception('Connection not started');
    if (this.IsConnected) {
        
        switch (cmd) {
            case PushServer.Commands.Init:
                this.handler.server.init(data);
                break;

            case PushServer.Commands.InitChat:
                this.handler.server.initChat(data);
                break;
                
            case PushServer.Commands.Messages:
                for (var i in data) {
                    var item = data[i];
                    this.handler.server.sendMessage(item.chatId, item.message);
                }
                break;

            case PushServer.Commands.AddView:
                console.log('Send AddView');
                console.log(data);
                this.handler.server.addView(data.chatId, data.type, data.id);
                break;

            case PushServer.Commands.Vote:
                console.log('Send Vote');
                console.log(data);
                this.handler.server.vote(data.chatId, data.type, data.id, data.vote);
                break;

            case PushServer.Commands.GetChatters:
                console.log('GetChatters');
                console.log(data);
                this.handler.server.getChatters(data.chatId);
                break;
        }

        //this.Sock.send(this.preparePacket(cmd, data));
    }
    else if (isGuarantee) {
        var that = this;
        setTimeout(function() {
            that.send(cmd, data, isGuarantee);
        }, 1000);
    }
};

PushServer.prototype.onConnect = function(callback) {
    this.OnConnectCallbacks.push(callback);
    if (this.IsConnected)
        callback.call(this);
};

PushServer.prototype.onDisconnect = function (callback) {
    this.OnDisconnectCallbacks.push(callback);
    if (!this.IsConnected)
        callback.call(this);
};

PushServer.prototype.subscribe = function(cmd, callback) {
    if (typeof cmd == 'object') {
        for (var i in cmd) {
            this.subscribe(i, cmd[i]);
        }
    } else {
        if (!this.Subscribers[cmd])
            this.Subscribers[cmd] = [];
        this.Subscribers[cmd].push(callback);
    }
};

App.PushServer = new PushServer();
App.PushServer.start();

/* Web Socket transport */
/*
var WsTransport = function (groupId, transportType, onData, onState) {
    this.onData = onData;
    this.onState = onState;
    this.groupId = groupId;
    this.handler = null;
    this.type = transportType;
    
    this.OnConnectCallbacks = [];
    this.Subscribers = {};
    this.Url = url;
    //this.Sock = null;
    this.IsConnected = false;
    
    var that = this;

    if (this.type == 'sockjs') {
        var sock = this.handler = new SockJS('http://localhost:3000/chat');
        sock.onopen = function () {
            //chatFrame.prepend($('<div/>').text('Connected'));
            //
            that.onInit();
            
            //NEW
            that.IsConnected = true;
            for (var i in that.OnConnectCallbacks) {
                var callback = that.OnConnectCallbacks[i];
                callback.call(that);
            }
        };

        sock.onmessage = function (e) {
            console.log(e.data);
            var message = JSON.parse(e.data);
            that.onData(message);
        };

        sock.onclose = function () {
            //chatFrame.prepend($('<div/>').text('Disconnected'));
        };
    }
    else if (this.type == 'signalr') {
        var hub = this.handler = $.connection.chatHub;

        hub.client.sendMessage = function (message) {
            //console.log(message);
            that.onData(
                WsTransport.preparePacket(WsTransport.commands.Messages, [message])
            );
        };

        hub.client.sendMessages = function (messages) {
            //console.log(message);
            that.onData(
                WsTransport.preparePacket(WsTransport.commands.Messages, messages)
            );
        };

        hub.client.updateChannelVideo = function () {
            //console.log(message);
            that.onData(
                WsTransport.preparePacket(WsTransport.commands.UpdateChannelVideo)
            );
        };
        
        hub.client.userConnected = function (user) {
            that.onData(
                WsTransport.preparePacket(WsTransport.commands.UserConnected, user)
            );
        };
        
        hub.client.userDisconnected = function (user) {
            that.onData(
                WsTransport.preparePacket(WsTransport.commands.UserDisconnected, user.Id)
            );
        };
        
        $.connection.hub.stateChanged(function (change) {
            console.log('new State' + change.newState);
            if (change.newState === $.signalR.connectionState.disconnected) {
                if (that.onState)
                    that.onState('disconnected');
                $.connection.hub.start();
            }
            if (change.newState === $.signalR.connectionState.reconnecting) {
                if (that.onState)
                    that.onState('disconnected');
                console.log('Re-connecting');
            } else if (change.newState === $.signalR.connectionState.connected) {
                console.log('The server is online');
                if (that.onState)
                    that.onState('connected');
            }
        });

        $.connection.hub.error(function (error) {
            console.log('error ' + error);
            if (that.onState)
                that.onState('error');
        });
        $.connection.hub.logging = true;
        
        $.connection.hub.reconnected(function () {
            console.log('Reconnected');
            //hub.server.increment(0);
        });

        $.connection.hub.start().done(function () {
            console.log('hub started');
            if (that.onInit)
                that.onInit();
            //hub.server.increment(0);
        });
    }
};

WsTransport.prototype.onInit = function () {
    this.send(WsTransport.preparePacket(WsTransport.commands.Init, { chat: this.groupId }));
};


WsTransport.prototype.send = function (message) {
    if (this.handler) {
        if (this.type == 'sockjs')
            this.handler.send(JSON.stringify(message));
        else if (this.type == 'signalr') {
            switch (message.cmd) {
                case WsTransport.commands.Init:
                    this.handler.server.init(message.data.chat);
                    break;

                case WsTransport.commands.Messages:
                    for (var i in message.data) {
                        var item = message.data[i];
                        this.handler.server.sendMessage(this.groupId, item.message);
                    }
                    break;
            }
        }
    }
};

WsTransport.commands = {
    Init: 1,
    Messages: 2,
    UpdateChannelVideo: 3,
    UserConnected: 4,
    UserDisconnected: 5
};

WsTransport.preparePacket = function(cmd, data) {
    return { cmd: cmd, data: data };
};*/



function loadVideo($container, position, videoUrl, mute, adsOff, stickers) {

    if (mute === null || mute === undefined) {
        mute = position > 0;
    }

    var playerId = 'player-' + position;

    $container.append($('<div/>', { id: playerId }));

    var params = {
        file: videoUrl,
        //width: position === 0 ? 474 : 118,
        //height: position === 0 ? 354 : 77,
        volume: mute ? 0 : 100,
        //mute: mute
    };

    if (position > 0) {
        params.advertising = null;
        params.controls = false;//TODO: test
        //params.mute = true;
    }
    else {
        //params.mute = false;
        //params.advertising = null;//TODO: test
    }
    
    if (adsOff) {
        params.advertising = null;
    }

    App.VideoPlayer.Set(playerId, params);
    //App.VideoPlayer.Mute(playerId, mute); //fix jwplayer cookies
    App.VideoPlayer.Volume(playerId, mute ? 0 : 100);   //fix jwplayer cookies
    
    if (stickers) {
        if(stickers.WebshowName)
            addPresenterWebshowOverlay($container, stickers.WebshowName);
        if (stickers.UserName)
            addPresenterNameOverlay($container, stickers.UserName);
    }

}

function muteVideo(position, flag) {
    var playerId = 'player-' + position;
    App.VideoPlayer.Volume(playerId, flag ? 0 : 100);
    //App.VideoPlayer.Mute(playerId, flag);
}

/* LOAD DEFAULT IMAGE IF SRC NOT EXISTS */

function handleImageErrors($container) {
    var $elements = $container
        ? $container.find('img[data-default-src]')
        : $('img[data-default-src]');
    
    $elements.filter('[src=""],:not([src])').each(function () {
        $(this).attr('data-loaded', '1').attr('src', $(this).attr('data-default-src'));
    });

    $elements.on('error', function () {
        if (!$(this).attr('data-loaded')) {
            $(this).attr('data-loaded', '1');
            $(this).attr('src', $(this).attr('data-default-src'));
        }
    }); 
}

/* LOAD MORE BUTTON AND PAGINATION */

function handleTabsPagination($tabs) {
    //Not supported now
    return;

    if (!$tabs)
        $tabs = $('a[data-toggle="tab"][data-limit][data-href]');

    $tabs.each(function() {
        var $tab = $(this);
        var href = $tab.attr('href');
        var $target = $(href);
        var itemsLimit = $tab.attr('data-limit');

        if (itemsLimit) {
            if ($target.find('.counted-item').length >= itemsLimit) {
                var $loadMoreBtn = $target.find('.load-more');
                if ($loadMoreBtn.length === 0) {
                    var $block = $('<div/>').addClass('row')
                        .append(
                            $('<div/>').addClass('col-xs-12')
                            .append(
                                $('<div/>').addClass('text-center load-more')
                                .append(
                                    $('<button/>').attr('data-page', 1)
                                                .addClass('btn btn-main-inverse')
                                                .text('Load more')
                                                .click(function () {
                                                    var $btn = $(this);
                                                    var nextPage = $btn.attr('data-page');
                                                    
                                                    $btn.attr('disabled', 'disabled');
                                                    var $row = $btn.closest('.row');
                                                    $.get($tab.attr('data-href'), { page: nextPage }, function (html) {
                                                        var $html = $(html);
                                                        handleImageErrors($html);
                                                        $row.before(html);
                                                        nextPage++;

                                                        if ($target.find('.counted-item').length >= nextPage * itemsLimit) {
                                                            $btn.attr('data-page', nextPage);
                                                            $btn.removeAttr('disabled');
                                                        }
                                                        else {
                                                            $row.hide();
                                                        }
                                                    });
                                                })
                                )
                            )
                        );

                    $target.append($block);
                }
            }
        }
    });
}

/* BOOTSTRAP MODAL AND  AJAX FORM */

var $modalCommon = $('#modal-common');
var $modalCommonContainer = $modalCommon.children();

function handleAjaxForm($forms) {
    $forms.each(function() {
        var $form = $(this);
        $form.find('.datepicker').datepicker({
            dateFormat: 'mm/dd/yy',
            changeYear: true,
            yearRange: 'c-100:c+10'
        });
        $form.find('.datetimepicker').datetimepicker({
            //defaultValue: '+1h'
        });

        $.validator.unobtrusive.parse($form);
        var $errors = $form.find('.form-errors');

        $form.ajaxForm({
            dataType: 'json',
            beforeSubmit: function (formData, jqForm, options) {
                $form.find('input[type="submit"]').attr('disabled', 'disabled');
            },
            success: function (result) {
                console.log(result);
                $errors.empty();
                if (result.Errors) {
                    for (var i in result.Errors) {
                        $errors.append($('<div/>').text(result.Errors[i]));
                    }
                    $form.find('input[type="submit"]').removeAttr('disabled');
                }
                else {
                    var callback = $form.data('ajax-success');
                    if (callback)
                        window[callback](result, $modalCommon, $form);
                }
                //$form.find('input[type="submit"]').removeAttr('disabled');
            },
            error: function (error) {
                console.log('Error');
                console.log(error);
                $errors.empty().append($('<div/>').text('Error'));
                $form.find('input[type="submit"]').removeAttr('disabled');
            }
        });
    });
}

function loadModal(url) {
    $modalCommonContainer.empty();
    $modalCommon.modal().addClass('loader');
    $modalCommonContainer.load(url, function (data) {
        $modalCommon.removeClass('loader');
        handleAjaxForm($modalCommonContainer.find('form[data-ajax]'));
    });
}

function changeElementState($el, newState, isOnClick) {
    var altText = $el.attr('data-alt-text');
    if (altText) {
        $el.attr('data-alt-text', $el.text());
        $el.text(altText);

        if (isOnClick) {
            var counter = $el.next().find('span');
            if (newState) {
                counter.text(+counter.text() + 1);
            } else {
                counter.text(+counter.text() - 1);
            }
        }
    }

    var altClass = $el.attr('data-alt-class');
    if (altClass) {
        newState ? $el.addClass(altClass) : $el.removeClass(altClass);
    }

    $el.attr('data-state', newState ? '1' : '0');
}

function handleChangeStateBtn ($container) {
    
    var $elements = $container
        ? $container.find('[data-change-state]')
        : $('[data-change-state]');

    $elements.attr('data-check-auth', 1);

    $elements.click(function () {
        var $this = $(this);
        var curState = $this.attr('data-state') == '1';
        $.post($(this).attr('data-change-state'),
            {
                state: !curState
            },
            function (data) {
                changeElementState($this, !curState, true);
            }
        );
    });

    $elements.each(function () {
        var $this = $(this);
        var curState = $this.attr('data-state') == '1';
        if (curState)
            changeElementState($this, true);
    });
}



function handleCarousel($container) {

    var $elements = $container
        ? $container.find('.carousel')
        : $('.carousel');

    $elements.each(function () {

        if ($(this).parent().attr('data-nocarousel'))
            return;

        var settings = {
            loop: false,
            margin: 0,
            nav: true,
            items: 3,
            navText: ['', ''],
            dots: true,
            dotsEach: 3,
        };
        //console.log('carousel');
        //console.log($(this).data());
        
        for (var d in $(this).data()) {
            if(d.indexOf('carousel') > -1){
                settings[d.replace('carousel', '').toLowerCase()] = $(this).data()[d];
            }
        }



        $(this).on('initialized.owl.carousel', function (event) {
            console.log('initialized.owl.carousel');
            console.log(event);
            var $target = $(event.target);
            var $nav = $target.find('.owl-nav');
            $nav.children().hide();
            $target.find('.owl-dots > *').hide();
            $target.find('.owl-prev').append($('<i/>').addClass('sficon-arrow-left'));
            $target.find('.owl-next').append($('<i/>').addClass('sficon-arrow-right'));

            if (event.item.count > event.page.size) {
                $nav.children('.owl-next').show();
                $(event.target).find('.owl-dots > *').show();
            }

        }).owlCarousel(settings).on('changed.owl.carousel', function (event) {
            console.log('changed.owl.carousel');
            console.log(event);
            var $nav = $(event.target).find('.owl-nav');
            $nav.children().show();
            if (event.item.index === 0) {
                $nav.children('.owl-prev').hide();
            }
            else if (event.item.index >= event.item.count - event.page.size) {
                $nav.children('.owl-next').hide();
            }
        });
    });
}

function handleCheckAuth($container) {
    if (!App.isAuth) {
        var $elements = $container
            ? $container.find('[data-check-auth]')
            : $('[data-check-auth]');

        $elements.each(function () {
            $(this).off().attr('data-modal', App.Settings.loginModalUrl)
                .click(function(e) {
                    e.preventDefault();
                });
        });

        handleModals($container);
    }
}

function handleModals($container) {

    var $elements = $container
                        ? $container.find('[data-modal]')
                        : $('[data-modal]');
    $elements.click(function () {
        loadModal($(this).data('modal'));
    });
}

function handleTabs($container) {

    var $elements = $container
                        ? $container.find('.nav-tabs-custom a[data-toggle="tab"]')
                        : $('.nav-tabs-custom a[data-toggle="tab"]');

    $elements.on('shown.bs.tab', function (e) {
        //console.log(e.target);
        var $this = $(e.target);
        var href = $this.data('href');

        if ($this.data('href') && !$this.data('loaded')) {
            var $target = $($this.attr('href')).addClass('loader');

            $target.load(href, function (data) {
                //console.log($(this));
                $target.removeClass('loader');
                $this.attr('data-loaded', 1);

                $(this).find('.videoHolder').each(function () {
                    var url = $(this).data('url');
                    loadVideo($(this), ++counter, url);
                });

                handleDates($target);

                handleImageErrors($target);

                handleTabsPagination($this);
                handleAjaxForm($target.find('form[data-ajax]'));
                handleModals($target);
                handleChangeStateBtn($target);
                handleCheckAuth($target);
                handleCarousel($target);
                handleTabs($target);
            });
        }
    });
}

function handleDates($container) {

    var $elements = $container
                        ? $container.find('[data-timestamp]')
                        : $('[data-timestamp]');

    $elements.each(function() {
        $(this).text(moment($(this).attr('data-timestamp')).format($(this).attr('data-format')));
    });
}


$(document).ready(function() {
    var counter = 10000;

    handleImageErrors();
    handleTabsPagination();
    handleAjaxForm($('form[data-ajax]'));
    handleModals();
    handleChangeStateBtn();
    handleCheckAuth();
    handleDates();
    handleCarousel();
    handleTabs();

    /* Notifications */

    $('#dropdown-notifications').on('show.bs.dropdown', function(e) {
        var $this = $(e.target);
        var href = $this.data('href');

        if (href && !$this.data('loaded')) {
            var $container = $this.find('.dropdown-menu').addClass('loader');
            
            $container.load(href, function () {
                $container.removeClass('loader');
                $this.attr('data-loaded', 1);
                $container.find('.timeago').each(function(key, value) {
                    var $value = $(value);
                    var rawDate = $value.attr('title');
                    $value.text(moment(rawDate).fromNow());
                });

                handleChangeStateBtn($container);
            });
        }
    });

    /* VIDEO TABS */   

    var hash = window.location.hash || history.state;
    if (hash && hash.indexOf('=') === -1) {        
        $('.history-tab a[href=' + hash + ']').tab('show');
    }

    $('.nav-tabs.history-tab a').on('shown.bs.tab', function (e) {
        if (history) {
            history.pushState(e.target.hash, '', location.pathname + e.target.hash);
        }
    });

    window.onpopstate = function (event) {
        $('.history-tab a[href=' + event.state + ']').tab('show');
    }

    $('.categories .tab-content-custom > :first .videoHolder').each(function () {
        var url = $(this).data('url');
        loadVideo($(this), ++counter, url);
    });

    /* BOOTSTRAP MODAL WITH AJAX FORM */


    $modalCommonContainer.delegate('[data-modal]', 'click', function () {
        var url = $(this).data('modal');
        $modalCommon.on('hidden.bs.modal', function (e) {
            $modalCommon.off('hidden.bs.modal');
            loadModal(url);
        }).modal('hide');
    });

    /* SEARCH */

    $('#search-toggler').click(function() {
        $('#search-form').toggle('slide', { direction: 'left' }, 500);
    });

    var shareConfig = {
        networks: {
            facebook: {
                before: function () {
                    this.url = shareDataConfig.BroadcastUrl;
                },
                app_id: shareDataConfig.FacebookAppId
            },
            twitter: {
                before: function () {
                    this.url = shareDataConfig.BroadcastUrl;
                    this.description = 'EN DIRECTO ' + shareDataConfig.WebshowName + ' por';
                    console.log(this.title);
                }
            }
        }
    };

    var share = new Share('.share-button', shareConfig);
});

/* Popove hover fix */

if ($.fn.popover) {

    var originalLeave = $.fn.popover.Constructor.prototype.leave;
    $.fn.popover.Constructor.prototype.leave = function(obj) {
        var self = obj instanceof this.constructor ?
            obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
        var container, timeout;

        originalLeave.call(this, obj);

        if (obj.currentTarget) {
            container = $(obj.currentTarget).siblings('.popover')
            timeout = self.timeout;
            container.one('mouseenter', function() {
                //We entered the actual popover – call off the dogs
                clearTimeout(timeout);
                //Let's monitor popover content instead
                container.one('mouseleave', function() {
                    $.fn.popover.Constructor.prototype.leave.call(self, self);
                });
            });
        }
    };

    $('.people [data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'hover',
        delay: { show: 50, hide: 100 },
        html: true,
        content: function() {
            var content = $(this).data('popover');
            if (!content) {
                $(this).data('popover', $(this).children('.popover-content').children());
                content = $(this).data('popover');
            }
            return content;
        }
    }).on('hide.bs.popover', function() {
        $(this).data('popover', $(this).siblings('.popover').children('.popover-content').children().clone(true));
    });

}

$('#invite-friends-button, .invite-friends').click(function () {
    FB.ui({
        method: 'send',
        link: window.location.href,
        redirect_uri: window.location.href
    });
});

$('#dropdown-notifications').click(function () {
    var dropdown = $(this);
    if (dropdown.data('open')) {
        dropdown.data('open', false);        
    } else {
        dropdown.data('open', true);
        // Currently we show at most 10 notifications
        var notificationsInPopup = 10;
        var notificationHolder = $('#notifications-counter-holder');
        var totalNotifications = +notificationHolder.text();
        var currentNotifications = totalNotifications - notificationsInPopup;
        if (currentNotifications <= 0) {
            $('.notifications-counter').addClass('hide');
            currentNotifications = 0;
        }
        notificationHolder.text(currentNotifications);
    }
});

function addUserLogoOverlay(videoHolder, username) {
    var userLogoOverlayContainer = $('<div class="user-logo-overlay"/>');
    //userLogoOverlayContainer.append($('<img class="user-overlay-image" src="/Content/images/player-user-logo.png" alt="user-logo"/>'));
    userLogoOverlayContainer.append($('<span class="user-overlay-name"/>').text(username));
    videoHolder.append(userLogoOverlayContainer);
}

function addPresenterWebshowOverlay(videoHolder, channelName) {
    var presenterWebshowOverlay = $('<div class="presenter-webshow-logo-overlay"/>');
    presenterWebshowOverlay.append($('<img class="presenter-webshow-overlay-image" src="/Content/images-new/indian_railway.svg" alt="webshow-logo"/>'));
    presenterWebshowOverlay.append($('<span class="presenter-webshow-overlay-label"/>').text('LIVE'));
    //presenterWebshowOverlay.append($('<span class="presenter-webshow-overlay-name"/>').text(channelName));
    videoHolder.append(presenterWebshowOverlay);
}

function addPresenterNameOverlay(videoHolder, username) {
    var presenterNameOverlay = $('<div class="presenter-logo-overlay"/>');
    //presenterNameOverlay.append($('<img class="presenter-overlay-image" src="/Content/images/player-presenter-name-overlay.png" alt="presenter-logo"/>'));
    presenterNameOverlay.append($('<span class="presenter-overlay-name"/>').text(username));
    videoHolder.append(presenterNameOverlay);
}

function updateSharingData(broadcast) {
    if (broadcast) {
        var baseUrl = window.location.protocol + '//' + window.location.host;

        shareDataConfig.UserName = broadcast.UserName;
        shareDataConfig.WebshowName = broadcast.WebshowName;
        shareDataConfig.SnapshotUrl = broadcast.SnapshotUrl;

        if (broadcast.Id) {
            shareDataConfig.BroadcastUrl = baseUrl + '/webshow/' + broadcast.WebshowName + '#broadcast=' + (broadcast.Id || 0);
        } else {
            shareDataConfig.BroadcastUrl = window.location.href;
        }

        shareDataConfig.FacebookAppId = $('*[property="fb:app_id"]').attr('content');
    }
}

//$('#animated-text-landing').typed({
//    strings: ['THE THINGS', 'CONCERTS', 'SPORTS', 'FASHION', 'ARTISTS', 'CELEBRITIES'],
//    contentType: 'text',
//    loop: true,
//    typeSpeed: 80,
//    backSpeed: 70,
//    backDelay: 1000
//});

$.stellar({
    horizontalScrolling: false
});