﻿using System.Web.Optimization;

namespace SayYeahTV.Web
{
    public class BundleConfig
    {
        #region Methods

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Content/jquery-ui/jquery-ui.min.js",
                "~/Content/jquery-ui/plugins/jquery-ui-timepicker-addon.js",
                //"~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.form.min.js",
                "~/Scripts/moment.js",
                "~/Scripts/owl.carousel.min.js",
                "~/Scripts/jquery.nicescroll.min.js",
                "~/Scripts/share.min.js",
                "~/Scripts/typed.min.js",
                "~/Scripts/jquery.stellar.js"
                ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/bootstrap/js/bootstrap.min.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                "~/Scripts/swfobject.js",
                "~/Content/jwplayer/jwplayer.js",
                "~/Scripts/Site.js"));

            bundles.Add(new ScriptBundle("~/bundles/geoposition").Include(
                "~/Content/geoposition/oms.min.js",
                "~/Content/geoposition/markerclusterer_compiled.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                "~/Scripts/jquery.signalR-2.2.0.min.js"
                //"~/signalr/hubs"
                ));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/jquery-ui/jquery-ui.css", new CssRewriteUrlTransform())
                .Include("~/Content/jquery-ui/jquery-ui.theme.css", new CssRewriteUrlTransform())
                .Include("~/Content/jquery-ui/plugins/jquery-ui-timepicker-addon.min.css")
                .Include("~/Content/icons.css")
                .Include("~/Content/bootstrap/css/bootstrap.min.css")
                .Include("~/Content/owl-carousel/owl.carousel.css")
                );

            bundles.Add(new LessBundle("~/Content/sitecss").Include(
                "~/Content/bootstrap-custom.less",
                "~/Content/Site.less"
                ));

            bundles.Add(new StyleBundle("~/Content/mobilecss").Include(
                "~/Content/mobile/mobile.css"
                ));
        }

        #endregion
    }
}