﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using SayYeahTv.Dependencies.Communications.Implementations;
using SayYeahTv.Dependencies.Communications.Interfaces;
using SayYeahTv.Dependencies.Modules;
using SayYeahTv.Dependencies.Services;
using SayYeahTv.Dependencies.Services.Interfaces;
using SayYeahTV.DataAccess;
using SayYeahTV.Management.Interfaces;
using SayYeahTV.Management.Managers;
using SayYeahTV.Web.Filters;

namespace SayYeahTV.Web.App_Start
{
    public class DependenciesConfig
    {
        #region Methods

        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new LoggingModule());
            builder.RegisterModule(new MessagingQueueModule());
            builder.RegisterModule(new RepositoryModule());

            builder.RegisterType<SayYeahTVContext>().AsSelf().InstancePerRequest();
            //builder.Register(x => GlobalHost.ConnectionManager.GetHubContext<ChatHub>()).As<IHubContext>();
            builder.RegisterType<NamesGenerator>().As<INamesGenerator>();
            builder.RegisterType<SayYeahTv.Dependencies.Services.ImageProcessor>().As<IImageProcessor>();
            builder.RegisterType<RecordingManager>().As<IRecordingManager>();
            builder.RegisterType<EventManager>().As<IEventManager>();
            builder.RegisterType<UserManager>().As<IUserManager>();
            builder.RegisterType<ChannelManager>().As<IChannelManager>();
            builder.Register(c =>
            {
                IEnumerable<string> euroUnionCountryCodes;
                using (var dbContext = new SayYeahTVContext())
                {
                    euroUnionCountryCodes = dbContext.SystemParameters.Single(
                        sp => sp.Tag == "EuroUnionCountryCodes")
                        .Value.Split(',');
                }
                return new MaxMindGeoIpService(string.Format("{0}/MaxMind/GeoLite2-Country.mmdb",
                                        AppDomain.CurrentDomain.GetData("DataDirectory")), euroUnionCountryCodes);
            }).As<IGeoIpService>().SingleInstance();
            builder.Register(
                x => new AzureBlobHelper(ConfigurationManager.AppSettings["CdnEndpoint"],
                        ConfigurationManager.AppSettings["StorageConnectionString"])).As<IAzureBlobHelper>();

            builder.RegisterType<EmailTemplateService>().As<IEmailTemplateService>().SingleInstance();
            builder.Register(c =>
            {
                using(var dbContext = new SayYeahTVContext())
                {
                    var sendGridUsername =
                        dbContext.SystemParameters.Single(sp => sp.Tag == "SendGridUsername").Value;
                    var sendGridPassword =
                        dbContext.SystemParameters.Single(sp => sp.Tag == "SendGridPassword").Value;
                    var sendGridTemplateId =
                        dbContext.SystemParameters.Single(sp => sp.Tag == "SendGridDefaultTemplateId").Value;

                    return new SendGridEmailSender(sendGridUsername, sendGridPassword, sendGridTemplateId, c.Resolve<IEmailTemplateService>());
                }
            }).As<IEmailSender>().SingleInstance();

            builder.RegisterType<ExceptionsLoggingAttribute>()
                .AsExceptionFilterFor<Controller>()
                .InstancePerRequest();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));


        }

        #endregion
    }
}