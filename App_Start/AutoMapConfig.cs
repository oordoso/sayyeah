﻿using System.Web.Mvc;
using AutoMapper;
using SayYeahTv.Dependencies.Services;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.App_Start;
using SayYeahTV.Web.Mapping.Resolvers;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web
{
    /// <summary>
    ///     Class contains mapping configuration for AutoMapper library.
    /// </summary>
    public class AutoMapConfig
    {
        #region Methods

        /// <summary>
        ///     Configures mappings.
        /// </summary>
        public static void Configure()
        {
            Mapper.CreateMap<Channel, ChannelDto>()
                    .ForMember(x => x.Logo, options => options.MapFrom(y => y.Logo ?? FileHelper.GetUploadedFileUrl(FileHelper.DefaultChannelLogo)))
                    .ForMember(x => x.IsCurrentUserFollow, options => options.ResolveUsing<IsCurrentUserFollowChannelResolver>());

            Mapper.CreateMap<Broadcast, BroadcastDto>()
                .ForMember(x => x.Snapshot, options => options.MapFrom(
                            y => y.Snapshot?? FileHelper.GetUploadedFileUrl(FileHelper.DefaultBroadcastLogo)
                        ));

            Mapper.CreateMap<Recording, RecordingDto>()
                .ForMember(x => x.Snapshot, options => options.MapFrom(
                            y => y.Snapshot ?? FileHelper.GetUploadedFileUrl(FileHelper.DefaultBroadcastLogo)
                        ));

            Mapper.CreateMap<Event, EventDto>();
            Mapper.CreateMap<Category, CategoryDto>();

            Mapper.CreateMap<User, UserDto>()
                .ForMember(x => x.Name, options => options.MapFrom(y => y.UserName))
                .ForMember(x => x.FullName, options => options.MapFrom(y => string.Format("{0} {1}", y.FirstName, y.LastName)))
                .ForMember(x => x.Photo, options => options.MapFrom(y => y.Photo ?? FileHelper.GetUploadedFileUrl(FileHelper.DefaultUserLogo)))
                .ForMember(x => x.IsCurrentUserFollow, options => options.ResolveUsing<IsCurrentUserFollowResolver>());

            Mapper.CreateMap<Category, SelectListItem>()
                .ForMember(t => t.Value, options => options.MapFrom(s => s.CategoryId))
                .ForMember(t => t.Text, options => options.MapFrom(s => s.Name));

            Mapper.CreateMap<CategoryDto, SelectListItem>()
                .ForMember(t => t.Value, options => options.MapFrom(s => s.CategoryId))
                .ForMember(t => t.Text, options => options.MapFrom(s => s.Name));

            Mapper.CreateMap<Channel, SelectListItem>()
                .ForMember(t => t.Value, options => options.MapFrom(s => s.ChannelId))
                .ForMember(t => t.Text, options => options.MapFrom(s => s.Name));

            Mapper.CreateMap<ChannelDto, SelectListItem>()
                .ForMember(t => t.Value, options => options.MapFrom(s => s.ChannelId))
                .ForMember(t => t.Text, options => options.MapFrom(s => s.Name));

            Mapper.CreateMap<User, UserSettingsViewModel>()
                 .ForMember(x => x.FullName, options => options.MapFrom(y => string.Format("{0} {1}", y.FirstName, y.LastName)))
                 .ForMember(x => x.Birthday, options => options.Ignore()); 
                //.ForMember(d => d.Notifications, options => options.Ignore());

            Mapper.CreateMap<UserSettingsViewModel, User>()
                .ForMember(x => x.Birthday, options => options.Ignore()); 

            Mapper.CreateMap<Notification, NotificationDto>()
                .ForMember(d => d.CanFollow, s => s.ResolveUsing<NotificationCanFollowResolver>());
        }

        #endregion
    }
}