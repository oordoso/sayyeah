﻿using System.Web.Mvc;
using SayYeahTV.Web.Filters;

namespace SayYeahTV.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new MobileRedirectAttribute());            
        }
    }
}
