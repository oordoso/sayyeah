﻿using System;
using System.Configuration;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(SayYeahTVContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/"),
                ExpireTimeSpan = TimeSpan.FromDays(30),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

/*            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["TwitterAppId"]))
            {
                app.UseTwitterAuthentication(
                   ConfigurationManager.AppSettings["TwitterAppId"],
                   ConfigurationManager.AppSettings["TwitterClientSecret"]);
            }            */

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["FacebookAppId"]))
            {
                var facebookAuthenticationOptions = new FacebookAuthenticationOptions();
                facebookAuthenticationOptions.Scope.Add("email");
                facebookAuthenticationOptions.Scope.Add("user_friends");
                facebookAuthenticationOptions.AppId = ConfigurationManager.AppSettings["FacebookAppId"];
                facebookAuthenticationOptions.AppSecret = ConfigurationManager.AppSettings["FacebookClientSecret"];
                facebookAuthenticationOptions.Provider = new FacebookAuthenticationProvider
                {
                    OnAuthenticated = async context => context.Identity.AddClaim(new Claim("FacebookAccessToken", context.AccessToken))
                };
                facebookAuthenticationOptions.SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie;
                app.UseFacebookAuthentication(facebookAuthenticationOptions);
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["GooglePlusAppId"]))
            {
                var googlePlusOptions = new GoogleOAuth2AuthenticationOptions
                {
                    ClientId = ConfigurationManager.AppSettings["GooglePlusAppId"],
                    ClientSecret = ConfigurationManager.AppSettings["GooglePlusClientSecret"]
                };
                googlePlusOptions.Scope.Add("email");
                googlePlusOptions.Scope.Add("profile");
                googlePlusOptions.Provider = new GoogleOAuth2AuthenticationProvider
                {
                    OnAuthenticated = async context => context.Identity.AddClaim(new Claim("GoogleAccessToken", context.AccessToken))
                };
                googlePlusOptions.SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie;
                app.UseGoogleAuthentication(googlePlusOptions);
            }
        }
    }
}