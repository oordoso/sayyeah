﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SayYeahTV.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Profile",
                url: "profile/{name}",
                defaults: new { controller = "Profile", action = "Index", name = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Category",
                url: "category/{tag}",
                defaults: new { controller = "Category", action = "Index", tag = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "LiveNow",
                url: "live-now",
                defaults: new { controller = "Category", action = "LiveNow" }
            );

            routes.MapRoute(
                name: "WebShows",
                url: "webshows",
                defaults: new { controller = "Category", action = "WebShows" }
            );

            routes.MapRoute(
                name: "Channel",
                url: "webshow/{tag}",
                defaults: new { controller = "Channel", action = "Show" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
