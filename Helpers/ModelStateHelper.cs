﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace SayYeahTV.Web.Helpers
{
    public static class ModelStateHelper
    {
        public static IEnumerable Errors(this ModelStateDictionary modelState)
        {
            var list = new List<string>();
            if (!modelState.IsValid)
            {
                foreach (var obj in modelState.Where(obj => obj.Value.Errors.Any()))
                {
                    list.AddRange(obj.Value.Errors.Select(e => e.ErrorMessage));
                }
            }
            return list;
        }
    }
}