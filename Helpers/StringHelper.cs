﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace SayYeahTV.Web.Helpers
{
    public static class StringHelper
    {
        public const string DateTimeFormat = "MM/dd/yyyy HH:mm";
        public const string DateFormat = "MM/dd/yyyy";

        public static string Translate(string word)
        {
            return word;
        }

        public static bool ToDateTime(string word, out DateTime datetime)
        {
            return DateTime.TryParseExact(
                word, 
                DateTimeFormat,
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None,
                out datetime
            );
        }

        public static bool ToDate(string word, out DateTime datetime)
        {
            return DateTime.TryParseExact(
                word,
                DateFormat,
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None,
                out datetime
            );

        }
        public static string FromDateTime(DateTime datetime)
        {
            return datetime.ToString(DateTimeFormat, new CultureInfo("en-US"));
        }

        public static string FromDate(DateTime datetime)
        {
            return datetime.ToString(DateFormat, new CultureInfo("en-US"));
        }
    }
}