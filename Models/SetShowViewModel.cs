﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class SetShowViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        public int Category1 { get; set; }

        public int? Category2 { get; set; }

        public int? Category3 { get; set; }

        public HttpPostedFileBase Logo { get; set; }

        public HttpPostedFileBase Background { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}