﻿using System.Collections.Generic;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class NotificationsListViewModel
    {
        public IEnumerable<NotificationDto> Notifications { get; set; }
    }
}