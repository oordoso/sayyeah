﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class AbuseReportViewModel
    {
        [Required]
        public string UserId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        public short Type { get; set; }

        public string PresenterId { get; set; }

        public IEnumerable<SelectListItem> Types { get; set; }
    }
}