﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models
{
    public class UserSettingsViewModel
    {
        public string Id { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string FullName { get; set; }
        public GenderType Gender { get; set; }
        public string Location { get; set; }
        public bool AllowLocationDiscover { get; set; }
        public string About { get; set; }

        [Required]
        [Display(Name = "User Name")]
        [RegularExpression(@"^([\w]+)$", ErrorMessage = "User Name must contains only latin words and digits")]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public HttpPostedFileBase NewPhoto { get; set; }
        public string Birthday { get; set; }

        //public Dictionary<NotificationEventType, NotificationTypes> Notifications { get; set; }
    }

    public class NotificationTypes
    {
        #region Properties

        public bool Email { get; set; }
        public bool InSite { get; set; }
        public bool Sms { get; set; }

        #endregion
    }
}