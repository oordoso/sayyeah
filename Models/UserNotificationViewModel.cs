﻿using System.Collections.Generic;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models
{
    public class UserNotificationViewModel
    {
        #region Properties

        public Dictionary<NotificationEventType, bool> SmsNotificationSettings { get; set; }
        public Dictionary<NotificationEventType, bool> MailNotificationSettings { get; set; }
        public Dictionary<NotificationEventType, bool> InSiteNotificationSettings { get; set; }

        #endregion
    }
}