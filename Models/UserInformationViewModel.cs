﻿using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class UserInformationViewModel
    {
        #region Properties

        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public UserDto User { get; set; }
        public int WebshowsCount { get; set; }

        #endregion
    }
}
