﻿using Newtonsoft.Json;

namespace SayYeahTV.Web.Models.OAuth
{
    internal class GooglePlusUserInfo
    {
        #region Properties

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("family_name")]
        public string FamilyName { get; set; }
        [JsonProperty("given_name")]
        public string GivenName { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
        [JsonProperty("locale")]
        public string Locale { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("picture")]
        public string Picture { get; set; }
        [JsonProperty("verified_email")]
        public bool VerifiedEmail { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }

        #endregion
    }
}