﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class WebshowsTabModel
    {
        public List<ChannelDto> Channels { get; set; }
        public UserDto User { get; set; }
    }
}