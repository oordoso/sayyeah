﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class HomeViewModel
    {
        public bool SkipLanding { get; set; }        
        public ChannelDto Show { get; set; }

        public List<UserDto> Users { get; set; } 

        public Dictionary<CategoryDto, IEnumerable<BroadcastDto>> CategoryBroadcasts { get; set; }

        public IEnumerable<ChannelDto> Shows { get; set; }

        public bool IsChannelFollower { get; set; }

        public bool IsOwnerFollower { get; set; }
    }
}