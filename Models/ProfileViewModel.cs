﻿using System.Collections.Generic;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class ProfileViewModel
    {
        #region Properties

        public UserDto User { get; set; }

        public IEnumerable<ChannelDto> FollowingChannels { get; set; }

        public IEnumerable<UserDto> FollowingUsers { get; set; }

        public bool IsPresenter = false;

        public bool IsOwner = false;

        public bool IsFollower = false;

        #endregion
    }
}