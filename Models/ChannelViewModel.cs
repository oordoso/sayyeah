﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SayYeahTv.Dependencies.Models;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class ChannelViewModel
    {
        public ChannelDto Channel { get; set; }

        //public List<EventDto> Events { get; set; }

        public Dictionary<CategoryDto, IEnumerable<BroadcastDto>> CategoryBroadcasts { get; set; }


        ///////////////////////

        public List<UserDto> Users { get; set; }

        public IEnumerable<ChannelDto> Shows { get; set; }
        public GroupBroadcast CurrentBroadcast { get; set; }


        //public bool IsOwner { get; set; }

        //public bool IsChannelFollower { get; set; }

        //public bool IsOwnerFollower { get; set; }

        //public int? PrivateSessionId { get; set; }

        //public string ChatId { get; set; }

        //public int VideoFramesNumber { get; set; }
    }
}