﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class SearchResultViewModel
    {
        public string Term { get; set; }
        public List<UserDto> Users { get; set; }
        public IEnumerable<BroadcastDto> Broadcasts { get; set; }
    }
}