﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class SetEventViewModel
    {
        public int Id { get; set; }

        [Required]
        public int ShowId { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        //[DataType(DataType.DateTime)]
        //[DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:" + StringHelper.DateTimeFormat + "}")]
        public string StartDate { get; set; }

        public int TimeZoneOffsetMinutes { get; set; }

        [Required]
        public int Category1 { get; set; }

        public int? Category2 { get; set; }

        public int? Category3 { get; set; }

        public HttpPostedFileBase Logo { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        public IEnumerable<SelectListItem> Shows { get; set; }
    }
}