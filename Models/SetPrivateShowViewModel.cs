﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class SetPrivateShowViewModel
    {
        public int ChannelId { get; set; }

        public IEnumerable<UserDto> Candidates { get; set; }

        public ICollection<string> CandidatesAccepted{ get; set; }
    }
}