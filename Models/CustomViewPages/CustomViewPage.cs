﻿using System.Web.Mvc;
using SayYeahTV.DataAccess;
using SayYeahTV.Management.Interfaces;

namespace SayYeahTV.Web.Models.CustomViewPages
{
    public abstract class AuthViewPage : WebViewPage
    {
        public SayYeahTVContext DbContext { get; set; }
        public IGeoIpService GeoIpService { get; set; }
    }
}