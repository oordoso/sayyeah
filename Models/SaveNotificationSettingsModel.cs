using System.Collections.Generic;
using SayYeahTV.DataAccess.Enums;

namespace SayYeahTV.Web.Models
{
    public class SaveNotificationSettingsModel
    {
        public Dictionary<NotificationEventType, NotificationTypes> Notifications { get; set; }
    }
}