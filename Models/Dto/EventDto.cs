using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models.Dto
{
    public class EventDto
    {
        public int EventId { get; set; }

        public int ChannelId { get; set; }

        public DateTime CreationDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Logo { get; set; }

        public DateTime StartDate { get; set; }

        public ChannelDto Channel { get; set; }


        //public DateTime? EndDate { get; set; }

        //public EventTypes Type { get; set; }

        //public string Url { get; set; }


        public bool IsCurrentUserSubscribed { get; set; }

        public static IEnumerable<EventDto> GetAll(int? channelId = null, int? categoryId = null, string term = null, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var _db = new SayYeahTVContext())
            {
                var query = _db.Events.Where(x => x.StartDate > DateTime.UtcNow)
                                        .Include(x => x.Channel.User)
                                        .Include(x => x.Channel.Categories);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Users);

                if (channelId.HasValue)
                    query = query.Where(x => x.ChannelId == channelId.Value);

                if (categoryId.HasValue)
                    query = query.Where(x => x.Categories.Any(y => y.CategoryId == categoryId));

                if (!String.IsNullOrWhiteSpace(term))
                    query = query.Where(x => x.Title.Contains(term));

                query = query.OrderBy(x => x.StartDate);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<EventDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.IsCurrentUserSubscribed = x.Users != null && x.Users.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

    }
}
