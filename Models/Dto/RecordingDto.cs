using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models.Dto
{
    public class RecordingDto
    {
        public int RecordingId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Snapshot { get; set; }
        public string VideoUrl { get; set; }
        public BroadcastTypes? SourceType { get; set; }
        //public int Views { get; set; }
        public ChannelDto Channel { get; set; }
    }
}
