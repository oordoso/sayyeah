using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.Web.Models.Enums;

namespace SayYeahTV.Web.Models.Dto
{
    public class ChannelDto
    {
        public int ChannelId { get; set; }

        public string Tag { get; set; }

        public string Name { get; set; }

        public string Logo { get; set; }

        public ChannelTypes Type { get; set; }

        public virtual UserDto User { get; set; }

        public virtual ICollection<CategoryDto> Categories { get; set; }

        public bool IsCurrentUserFollow { get; set; }

        public string Background { get; set; }

        public int FollowersNumber
        {
            get
            {
                using (var db = new SayYeahTVContext())
                {
                    if (ChannelId == 0)
                        return 0;
                    var firstOrDefault = db.Channels.FirstOrDefault(x => x.ChannelId == ChannelId);
                    if (firstOrDefault == null)
                        return 0;
                    return firstOrDefault.Followers.Count;

                }
            }
        }

        public string GetLink()
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext).RouteUrl("Channel", new { tag = Tag });
        }

        public string GetEditLink()
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext).Action("SetShow", "Channel", new{showId = ChannelId});
        }


        public static IEnumerable<ChannelDto> GetShows(string currentUserId = null, string term = null, TabTypes type = TabTypes.All)
        {
            using (var db = new SayYeahTVContext())
            {
                var query = db.Channels
                        .Include(x => x.User)
                        .Include(x => x.Categories);

                if (!String.IsNullOrWhiteSpace(currentUserId) || type == TabTypes.Top)
                    query = query.Include(x => x.Followers);

                query = query.Where(x => x.Type == ChannelTypes.ChannelGroup && !x.IsPrivate && x.State == States.Active);

                if (!String.IsNullOrWhiteSpace(term))
                    query = query.Where(x => x.Name.Contains(term));

                switch (type)
                {
                    case TabTypes.All:
                        query = query.OrderBy(x => Guid.NewGuid());
                        break;

                    case TabTypes.Alphabet:
                        query = query.OrderBy(x => x.Name);
                        break;

                    case TabTypes.New:
                        query = query.OrderByDescending(x => x.ChannelId);
                        break;

                    case TabTypes.Top:
                        query = query.OrderByDescending(x => x.Followers.Count);
                        break;
                }

                if(type != TabTypes.Alphabet)
                    query = query.Take(12);

                return query.ToList()
                        .Select(x =>
                        {
                            var obj = Mapper.Map<ChannelDto>(x);
                            if (!String.IsNullOrWhiteSpace(currentUserId))
                                obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                            return obj;
                        });
            }
        }
    }
}
