using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace SayYeahTV.Web.Models.Dto
{
    public class BroadcastDto
    {
        public int BroadcastId { get; set; }

        public int ChannelId { get; set; }

        public int? ChannelGroupId { get; set; }

        public string UserId { get; set; }
        
        public string Name { get; set; }

        public string Snapshot { get; set; }

        public string VideoUrl { get; set; }

        //public int Views { get; set; }

        public UserDto User { get; set; }

        public ChannelDto Channel { get; set; }

        public ChannelDto ChannelGroup { get; set; }

        public static Dictionary<CategoryDto, IEnumerable<BroadcastDto>> GetBroadcastsPerCategory(string currentUserId = null, 
            bool presentersOnly = false)
        {
            using (var _db = new SayYeahTVContext())
            {
                var broadcasts = GetLiveBroadcasts(currentUserId, presentersOnly);

                var model = new Dictionary<CategoryDto, IEnumerable<BroadcastDto>>
                {
                    {new CategoryDto {Name = "Live Now"}, broadcasts}
                };

                /*
                _db.Categories.OrderBy(x => x.CategoryId)
                    .Take(4)
                    .ForEach(x => model.Add(Mapper.Map<CategoryDto>(x), null));
                    */
                return model;
            }
        }

        public static IEnumerable<BroadcastDto> GetLiveBroadcasts(string currentUserId = null, bool presentersOnly = false, int? limit = null)
        {
            using (var _db = new SayYeahTVContext())
            {
                var query = _db.Broadcasts.Where(
                        x => x.IsOnAir &&
                        x.Channel.State == States.Active
                            && x.Channels.Any(
                                c => c.ChannelMembers.Any(
                                    m => m.Position == 0 
                                        && m.IsPositionAccepted
                                        && m.IsAccepted
                                        && m.UserId == x.UserId
                                )
                            ) 
                    )
                    .Include(x => x.User)
                    .Include(x => x.MediaServer)
                    .Include(x => x.Channel.User)
                    .Include(x => x.Channel.Categories)
                    .Include(x => x.ChannelGroup.User)
                    .Include(x => x.ChannelGroup.Categories);

                query = query.Where(b => b.IsOnAir == true);

                if (presentersOnly)
                {
                    query = query.Where(b => b.UserId == b.ChannelGroup.User.Id);
                }

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Channel.Followers);

                query = query.OrderByDescending(x => x.BroadcastId);

                if (limit.HasValue)
                    query = query.Take((int)limit);

                var brList = query.ToList();

                return String.IsNullOrWhiteSpace(currentUserId)
                        ? Mapper.Map<IEnumerable<BroadcastDto>>(brList)
                        : brList.Select(x =>
                        {
                            var obj = Mapper.Map<BroadcastDto>(x);
                            obj.Channel.IsCurrentUserFollow = x.Channel.Followers != null &&
                                                              x.Channel.Followers.Any(y => y.Id == currentUserId);
                            return obj;
                        });
            }
        }

        public static IEnumerable<BroadcastDto> GetAll(int? channelId = null, int? categoryId = null, string term = null, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var _db = new SayYeahTVContext())
            {
                var query = _db.Broadcasts.Where(x => x.IsOnAir)
                                        .Include(x => x.MediaServer)
                                        .Include(x => x.Channel.User)
                                        .Include(x => x.Channel.Categories)
                                        .Include(x => x.ChannelGroup.User)
                                        .Include(x => x.ChannelGroup.Categories);

                if (channelId.HasValue)
                    query = query.Where(x => x.ChannelId == channelId.Value);

                if (categoryId.HasValue)
                    query = query.Where(x => x.Categories.Any(y => y.CategoryId == categoryId));

                if (!String.IsNullOrWhiteSpace(term))
                    query = query.Where(x => x.Name.Contains(term));

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Channel.Followers);

                query = query.OrderByDescending(x => x.BroadcastId);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<BroadcastDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.Channel.IsCurrentUserFollow = x.Channel.Followers != null && x.Channel.Followers.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }
    }
}
