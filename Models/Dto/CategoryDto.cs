using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models.Dto
{
    public class CategoryDto
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }

    }
}
