using System;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Models.Dto
{
    public class NotificationDto
    {
        #region Properties

        public DateTime DateTime { get; set; }
        public NotificationEventType EventType { get; set; }
        public string Image { get; set; }
        public string ProducerLink { get; set; }
        public string ProducerName { get; set; }
        public string TargetLink { get; set; }
        public string TargetName { get; set; }
        public string ProducerEntityId { get; set; }
        public EntityType? ProducerEntityType { get; set; }
        public EntityType? TargetEntityType { get; set; }
        public bool CanFollow { get; set; }

        #endregion
    }
}