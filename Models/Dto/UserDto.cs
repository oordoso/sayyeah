using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Mapping;
using SayYeahTV.Web.Models.Enums;

namespace SayYeahTV.Web.Models.Dto
{
    public class UserDto
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string FullName { get; set; }

        //public string Link { get; set; }

        public string Photo { get; set; }
        public string UserName { get; set; }
        public string About { get; set; }

        public int FollowersNumber
        {
            get
            {
                using (var db = new SayYeahTVContext())
                {
                    if (String.IsNullOrWhiteSpace(Id))
                        return 0;
                    var firstOrDefault = db.Users.FirstOrDefault(x => x.Id == Id);
                    if (firstOrDefault == null)
                        return 0;
                    return firstOrDefault.Followers.Count;
                    
                }
            }
        }

        public string GetLink()
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext).RouteUrl("Profile", new {name = Name});
        }

        public bool IsCurrentUserFollow { get; set; }

        public static IEnumerable<UserDto> GetAll(int? categoryId = null, string term = null, int limit = 0, int offset = 0, string currentUserId = null, TabTypes type = TabTypes.New)
        {
            using (var db = new SayYeahTVContext())
            {
                var userRoleName = Enum.GetName(typeof (Roles), Roles.User);
                var role = db.Roles.FirstOrDefault(x => x.Name == userRoleName);

                if(role == null)
                    return null;

                var roleId = role.Id;

                IQueryable <User> query = db.Users;

                if (!String.IsNullOrWhiteSpace(currentUserId) || type == TabTypes.Top)
                    query = query.Include(x => x.Followers);

                
                query = query.Where(x => x.Roles.Any(y => y.RoleId == roleId));

                if (categoryId.HasValue)
                    query = query.Where(x => x.Channels.Any(y => y.Categories.Any(z => z.CategoryId == categoryId)));

                if (!String.IsNullOrWhiteSpace(term))
                    query = query.Where(x => x.UserName.Contains(term) || x.FirstName.Contains(term) || x.LastName.Contains(term));

                switch (type)
                {
                    case TabTypes.All:
                        query = query.OrderBy(x => Guid.NewGuid());
                        break;

                    case TabTypes.New:
                        query = query.OrderByDescending(x => x.RegisterDateTime);
                        break;

                    case TabTypes.Top:
                        query = query.OrderByDescending(x => x.Followers.Count);
                        break;
                }


                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<UserDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

        public static IEnumerable<EventDto> GetUpcomings(string userId, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                var evt = db.Events.Where(x => x.Channel.User.Id == userId && x.StartDate > DateTime.UtcNow)
                        .Include(x => x.Channel.User)
                        .Include(x => x.Channel.Categories)
                        .Include(x => x.Users)
                        .OrderBy(x => x.StartDate)
                        .Take(10)
                        .ToList();

                return evt.Select(x =>
                    {
                        var obj = Mapper.Map<EventDto>(x);
                        obj.IsCurrentUserSubscribed = x.Users != null && x.Users.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

        public static IEnumerable<UserDto> GetChannelFollowers(int channelId, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                IQueryable<User> query = db.Users
                                            .Where(x => x.FollowingChannels.Any(y => y.ChannelId == channelId));

                query = query.OrderByDescending(x => x.Id);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<UserDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

        public static IEnumerable<UserDto> GetFollowers(string userId, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                IQueryable<User> query = db.Users
                                            .Where(x => x.Followings.Any(y => y.Id == userId));

                query = query.OrderByDescending(x => x.Id);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<UserDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

        public static IEnumerable<ChannelDto> GetFollowingChannels(string userId, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                IQueryable<Channel> query = db.Channels
                                                .Include(x => x.User)
                                                .Include(x => x.Categories);

                if (!String.IsNullOrWhiteSpace(userId))
                    query = query.Where(x => x.Followers.Any(y => y.Id == userId));

                query = query.OrderByDescending(x => x.ChannelId);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return Mapper.Map<IEnumerable<ChannelDto>>(query.ToList(), 
                    options => options.Items[MapperProperties.CurrentUserId] = currentUserId);
            }
        }

        public static IEnumerable<UserDto> GetFollowings(string userId, int limit = 0, int offset = 0, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                var query = db.Users.Where(x => x.Followers.Any(y => y.Id == userId));

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                query = query.OrderByDescending(x => x.RegisterDateTime);

                if (offset > 0)
                    query = query.Skip(offset);

                if (limit > 0)
                    query = query.Take(limit);

                return query.ToList()
                    .Select(x =>
                    {
                        var obj = Mapper.Map<UserDto>(x);
                        if (!String.IsNullOrWhiteSpace(currentUserId))
                            obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                        return obj;
                    });
            }
        }

        public static IEnumerable<ChannelDto> GetShows(string userId, string currentUserId = null)
        {
            using (var db = new SayYeahTVContext())
            {
                var query = db.Channels
                        .Include(x => x.User)
                        .Include(x => x.Categories);

                if (!String.IsNullOrWhiteSpace(currentUserId))
                    query = query.Include(x => x.Followers);

                query = query.Where(x => x.OwnerId == userId && x.Type == ChannelTypes.ChannelGroup && !x.IsPrivate && x.State == States.Active)
                                .OrderBy(x => x.ChannelId);

                return query.ToList()
                        .Select(x =>
                        {
                            var obj = Mapper.Map<ChannelDto>(x);
                            if (!String.IsNullOrWhiteSpace(currentUserId))
                                obj.IsCurrentUserFollow = x.Followers != null && x.Followers.Any(y => y.Id == currentUserId);
                            return obj;
                        });
            }
        }
    }
}
