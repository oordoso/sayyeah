﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Models
{
    public class RemoveRecordingViewModel
    {
        public int Id { get; set; }
    }
}