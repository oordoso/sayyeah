using System.Linq;
using AutoMapper;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Mapping.Resolvers
{
    public class NotificationCanFollowResolver : IValueResolver
    {
        #region IValueResolver Members

        public ResolutionResult Resolve(ResolutionResult source)
        {
            var notification = (Notification) source.Value;
            if (notification.ProducerEntityType != EntityType.User)
            {
                return source.New(false);
            }

            object userOut;
            if (!source.Context.Options.Items.TryGetValue("currentUser", out userOut))
            {
                throw new AutoMapperMappingException("Unable to resolve notification, user ID is null.");
            }

            var user = userOut as User;
            return source.New(!user.Followings.Any(f => f.Id == notification.ProducerEntityId));
        }

        #endregion
    }
}
