﻿using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Mapping.Resolvers
{
    public class IsCurrentUserFollowResolver : IValueResolver
    {
        #region IValueResolver Members

        //public ResolutionResult Resolve(ResolutionResult source)
        //{
        //    var user = (User) source.Context.SourceValue;

        //    object currentUserId;
        //    if (source.Context.Options.Items.TryGetValue(MapperProperties.CurrentUserId, out currentUserId))
        //    {
        //        var userId = (string) currentUserId;
        //        return source.New(!string.IsNullOrEmpty(userId) && user.Followers.Any(f => f.Id == userId));
        //    }

        //    return source.New(false);
        //}

        public ResolutionResult Resolve(ResolutionResult source)
        {
            var user = (User)source.Context.SourceValue;
            //var identityUser = HttpContext.Current.GetOwinContext().Authentication.User as  User;

            //if (identityUser == null) return source.New(false);
            using (var db = new SayYeahTVContext())
            {
                var userId = HttpContext.Current.User.Identity.GetUserId();
                return source.New(!string.IsNullOrEmpty(userId) && db.Users.First(i => i.Id == user.Id).Followers.Any(f => f.Id == userId));
            }
            
            
           
        }
        #endregion
    }
}
