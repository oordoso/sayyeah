﻿using System.Linq;
using AutoMapper;
using SayYeahTV.DataAccess.Models;

namespace SayYeahTV.Web.Mapping.Resolvers
{
    public class IsCurrentUserFollowChannelResolver : IValueResolver
    {
        #region IValueResolver Members

        public ResolutionResult Resolve(ResolutionResult source)
        {
            var channel = (Channel) source.Context.SourceValue;

            object currentUserId;
            if (source.Context.Options.Items.TryGetValue(MapperProperties.CurrentUserId, out currentUserId))
            {
                var userId = (string) currentUserId;
                return source.New(!string.IsNullOrEmpty(userId) && channel.Followers.Any(f => f.Id == userId));
            }

            return source.New(false);
        }

        #endregion
    }
}