﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SayYeahTv.Dependencies.Communications;
using SayYeahTv.Dependencies.Communications.Messages;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Mapping;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;

namespace SayYeahTV.Web.Controllers
{
    public class ProfileController : Controller
    {
        private readonly SayYeahTVContext _db;
        private readonly IMessageQueueManager<ReportAbuseMessage> _reportAbuseQueueManager;

        private const int WebShowsPerPage = 16;
        private const int FollowingsPerPage = 16;
        private const int FollowersPerPage = 5;
        private const int EventsPerPage = 16;
        

        public ProfileController(SayYeahTVContext db, IMessageQueueManager<ReportAbuseMessage> reportAbuseQueueManager)
        {
            _db = db;
            _reportAbuseQueueManager = reportAbuseQueueManager;
        }

        // GET: Profile
        public ActionResult Index(string name = null)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                var users = _db.Users.OrderByDescending(x => x.Id)
                                .Take(20);

                return View(Mapper.Map<List<UserDto>>(users));
            }

            //TODO : change url with Id
            var user = _db.Users.SingleOrDefault(x => x.UserName == name);

            if (user == null)
                throw new HttpException(404, "Not found");

            var userid = User.Identity.GetUserId();

            //ViewBag.WebShowsLimit = WebShowsPerPage;
            ViewBag.FollowingsLimit = FollowingsPerPage;
            ViewBag.FollowersLimit = FollowersPerPage;
            //ViewBag.EventsLimit = EventsPerPage;

            var followingChannels = UserDto.GetFollowingChannels(user.Id, limit: FollowingsPerPage, currentUserId: userid);
            var followingUsers = UserDto.GetFollowings(user.Id, limit: FollowingsPerPage, currentUserId: userid);

            //var presenterRole =_db.Roles.FirstOrDefault(r => r.Name == Roles.Presenter.ToString());

            //var presenterRoleId = presenterRole == null ? null : presenterRole.Id;

            var model = new ProfileViewModel
            {
                User = Mapper.Map<UserDto>(user),
                FollowingChannels = followingChannels,
                FollowingUsers = followingUsers,
                IsPresenter = user.IsInAnyRole(Roles.Presenter) //presenterRoleId != null && user.Roles.Any(x => x.RoleId == presenterRoleId)
            };


            if (!String.IsNullOrWhiteSpace(userid))
            {
                if (userid == user.Id)
                    model.IsOwner = true;

                else if (user.Followers.Any(y => y.Id == userid))
                    model.IsFollower = true;
            }

            return View("Info", model);
        }


        public ActionResult GetWebShows(string id/*, int page = 0*/)
        {
            var userid = User.Identity.GetUserId();
            var shows = UserDto.GetShows(id, currentUserId:userid);
            return PartialView("_ChannelsPartial", shows);
        }

        public ActionResult GetWebshowsTab(string id)
        {
            var user = _db.Users.Find(id);
            ViewBag.IsChannelFollower = true;
            ViewBag.IsOwnerFollower = false;
            var model = new WebshowsTabModel
            {
                User = Mapper.Map<UserDto>(user),
                Channels = Mapper.Map<List<ChannelDto>>(user.Channels.Where(c => c.Type == ChannelTypes.ChannelGroup
            && !c.IsPrivate && c.State == States.Active))
            };
            return PartialView("_WebshowsPartial", model);
        }

        public ActionResult GetFollowingTab(string id)
        {
            var currentUser = _db.Users.Find(id);
            ViewBag.UserId = id;
            return PartialView("_FollowingPartial", 
                Mapper.Map<IEnumerable<UserDto>>(currentUser.Followings.Where(user => !user.IsInAnyRole(Roles.Presenter)), 
                options => options.Items[MapperProperties.CurrentUserId] = User.Identity.GetUserId()));
        }

        public ActionResult GetFollowersTab(string id)
        {
            var currentUser = _db.Users.Find(id);
            ViewBag.UserId = id;
            return PartialView("_FollowersPartial", 
                Mapper.Map<IEnumerable<UserDto>>(currentUser.Followers.Where(user => !user.IsInAnyRole(Roles.Presenter)),
                options => options.Items[MapperProperties.CurrentUserId] = User.Identity.GetUserId()));
        }

        public ActionResult GetFollowedHostsTab(string id)
        {
            var currentUser = _db.Users.Find(id);
            return PartialView("_FollowingHosts", Mapper.Map<IEnumerable<ChannelDto>>(
                currentUser.FollowingChannels
                .Where(c => !c.IsPrivate && c.State == States.Active),
                options => options.Items[MapperProperties.CurrentUserId] = User.Identity.GetUserId()));
        }

        public ActionResult GetFollowedWebshowsTab(string id)
        {
            var userid = User.Identity.GetUserId();
            var shows = UserDto.GetFollowingChannels(id, currentUserId: userid);
            return PartialView("_FollowingHosts", shows);
        }

        public ActionResult GetFollowersHostsTab(string id)
        {
            var currentUser = _db.Users.Find(id);
            /*Show the most followed webshow for each host*/
            return PartialView("_FollowingHosts", 
                Mapper.Map<IEnumerable<ChannelDto>>(
                    currentUser.Followers
                .Where(user => user.IsInAnyRole(Roles.Presenter) && user.Channels.Any())
                .Select(s => s.Channels.OrderByDescending(c => c.Followers.Count)).First(),
                options => options.Items[MapperProperties.CurrentUserId] = User.Identity.GetUserId()));
        }

        public ActionResult GetFollowersWebshowsTab(string id)
        {
            var userid = User.Identity.GetUserId();
            var shows = UserDto.GetFollowingChannels(id, currentUserId: userid);
            return PartialView("_FollowingHosts", shows);
        }

        public ActionResult GetTopWebshowsTab(string id)
        {
            var channelDtos = UserDto.GetShows(id, User.Identity.GetUserId());
            return PartialView("_ChannelsPartial", channelDtos);
        }

        public ActionResult GetRecordingsTab(string id)
        {            
            var user = _db.Users.Find(id);
            return PartialView("_PreviousVideoPartial", Mapper.Map<IEnumerable<RecordingDto>>(user.Recordings));
        }

        public ActionResult GetUpcomingsTab(string id)
        {
            var eventDtos = UserDto.GetUpcomings(id, User.Identity.GetUserId());
            return PartialView("_UpcomingsPartial", eventDtos);
        }

        public ActionResult GetFollowings(string id, int page = 0)
        {
            var userid = User.Identity.GetUserId();
            var followings = UserDto.GetFollowings(id, limit: FollowingsPerPage, offset: page * FollowingsPerPage, currentUserId: userid);
            return PartialView("_UsersPartial", followings);
            //return PartialView("_ChannelsPartial", Mapper.Map<List<ChannelDto>>(followings));
        }

        public ActionResult GetFollowers(string id, int page = 0)
        {
            var userid = User.Identity.GetUserId();
            var followers = UserDto.GetFollowers(id, limit: FollowersPerPage, offset: page * FollowersPerPage, currentUserId: userid);
            return PartialView("_UsersFiveInRow", followers);
        }

        public ActionResult GetEvents(string id/*, int page = 0*/)
        {
            var events = UserDto.GetUpcomings(id, User.Identity.GetUserId());
            return PartialView("_EventsPartial", events);
        }

        public ActionResult UserInformation(string userid)
        {
            var currentUserId = User.Identity.GetUserId();
            var user = _db.Users.SingleOrDefault(x => x.Id == userid);
            if (user == null)
                return null;

            UserInformationViewModel model = new UserInformationViewModel();
            model.User = Mapper.Map<UserDto>(user);
            model.User.IsCurrentUserFollow = user.Followers != null && user.Followers.Any(f => f.Id == currentUserId);
            model.WebshowsCount = user.Channels.Count(c => !c.IsPrivate && c.Type == ChannelTypes.ChannelGroup && c.State == States.Active);
            model.FollowersCount = user.Followers.Count;
            model.FollowingCount = user.Followings.Count;            
            return PartialView("_UserInformation", model);
        }

        public ActionResult AbuseReport(string id, string presenterId)
        {
            //var channel = _db.Channels.FirstOrDefault(x => x.ChannelId == id);

            //if (channel == null)
                //return HttpNotFound();

            var types = new List<SelectListItem>();

            var values = Enum.GetValues(typeof(AbuseTypes));
            Array.Reverse(values);

            foreach( var val in values )
            {
                types.Add(new SelectListItem
                {
                    Value = ((short)val).ToString(),
                    Text = Enum.GetName(typeof(AbuseTypes), val),
                    Selected = false
                });
            }

            var model = new AbuseReportViewModel
            {
                UserId = id,
                Types = types,
                PresenterId = presenterId
            };

            return PartialView("_AbuseReportPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AbuseReport(AbuseReportViewModel model)
        {
            if(!User.Identity.IsAuthenticated)
                return Json(new { Errors = new[] { "Please Log In before" } }, JsonRequestBehavior.AllowGet);

            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            var type = (AbuseTypes) model.Type;

            if (type == AbuseTypes.Other && String.IsNullOrWhiteSpace(model.Description) )
                return Json(new { Errors = new[]{"Please add description"} }, JsonRequestBehavior.AllowGet);

            var reporterId = User.Identity.GetUserId();
            _db.AbuseReports.Add(new AbuseReport
            {
                ReporterId = reporterId,
                UserId = model.UserId,
                CreationDate = DateTime.UtcNow,
                Description = model.Description,
                Type = (AbuseTypes)model.Type
            });

            await _db.SaveChangesAsync();

            await _reportAbuseQueueManager.SendMessageAsync(new ReportAbuseMessage
            {
                UserId = model.UserId,
                PresenterId = model.PresenterId,
                ReporterId = reporterId
            });

            return Json(new { Success = true}, JsonRequestBehavior.AllowGet);
        }
    }
}