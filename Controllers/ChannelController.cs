﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SayYeahTv.Dependencies.Communications;
using SayYeahTv.Dependencies.Communications.Messages;
using SayYeahTv.Dependencies.Hubs;
using SayYeahTv.Dependencies.Models;
using SayYeahTv.Dependencies.Services.Interfaces;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Extensions;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Management.Interfaces;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;
using SayYeahTV.Web.Models.Enums;

namespace SayYeahTV.Web.Controllers
{
    public class ChannelController : Controller
    {
        //private readonly IHubContext _chatHub;
        private readonly SayYeahTVContext _db;
        private readonly INamesGenerator _namesGenerator;
        private readonly IMessageQueueManager<GotNewFollowerMessage> _gotNewFollowerMessageManager;
        private readonly IMessageQueueManager<WebshowSchedulesEventMessage> _webshowSchedulesEventMessageManager;
        private readonly IMessageQueueManager<PresenterSchedulesEventMessage> _presenterSchedulesEventMessageManager;
        private readonly IMessageQueueManager<PresenterCreatesWebshowMessage> _presenterCreatesWebshowMessageManager;
        private readonly IMessageQueueManager<PresenterFollowsWebshow> _presenterFollowsWebshowMessageManager;
        private readonly IMessageQueueManager<PresenterJoinsWebshowLong> _presenterJoinsWebshowLongMessageManager;
        private readonly IAzureBlobHelper _azureBlobHelper;
        private readonly IImageProcessor _imageProcessor;
        private readonly IChannelManager _channelManager;

        private const int EventsPerPage = 16;
        private const int RecordingsPerPage = 16;
        private const int FollowersPerPage = 16;
        private const int UsersPerPage = 24;


        public ChannelController(SayYeahTVContext db, /* IHubContext chatHub,*/ INamesGenerator namesGenerator,
            IMessageQueueManager<GotNewFollowerMessage> gotNewFollowerMessageManager,
            IMessageQueueManager<WebshowSchedulesEventMessage> webshowSchedulesEventMessageManager,
            IMessageQueueManager<PresenterSchedulesEventMessage> presenterSchedulesEventMessageManager,
            IMessageQueueManager<PresenterCreatesWebshowMessage> presenterCreatesWebshowMessageManager,
            IMessageQueueManager<PresenterFollowsWebshow> presenterFollowsWebshowMessageManager,
            IMessageQueueManager<PresenterJoinsWebshowLong> presenterJoinsWebshowLongMessageManager,
            IAzureBlobHelper azureBlobHelper, IImageProcessor imageProcessor, IChannelManager channelManager)
        {
            //_chatHub = chatHub;
            _db = db;
            _namesGenerator = namesGenerator;
            _gotNewFollowerMessageManager = gotNewFollowerMessageManager;
            _webshowSchedulesEventMessageManager = webshowSchedulesEventMessageManager;
            _presenterSchedulesEventMessageManager = presenterSchedulesEventMessageManager;
            _presenterCreatesWebshowMessageManager = presenterCreatesWebshowMessageManager;
            _presenterFollowsWebshowMessageManager = presenterFollowsWebshowMessageManager;
            _presenterJoinsWebshowLongMessageManager = presenterJoinsWebshowLongMessageManager;
            _azureBlobHelper = azureBlobHelper;
            _imageProcessor = imageProcessor;
            _channelManager = channelManager;
        }

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Show(string tag/*, int? privateSessionId = null*/)
        {            
            var channel = _channelManager.GetByKey(tag: tag);

            var channelDto = Mapper.Map<ChannelDto>(channel);

            if (channel == null)
                throw new HttpException(404, "Not found");

            ViewBag.EventsLimit     = EventsPerPage;
            ViewBag.RecordingsLimit = RecordingsPerPage;
            ViewBag.FollowersLimit  = FollowersPerPage;

            // Properties for ads
            ViewBag.Presenter = channelDto.User.Name;
            ViewBag.Episode = channelDto.Name;
            ViewBag.Category = channelDto.Categories;

            var userid = User.Identity.GetUserId();

            //var events = EventDto.GetAll(channelId: channel.ChannelId, limit: EventsPerPage, currentUserId: userid);

            //var eventsDto = Mapper.Map<List<EventDto>>(events);
            var users = UserDto.GetAll(limit: UsersPerPage, currentUserId: userid, type: TabTypes.All).ToList();

            //var eventsDto = EventDto.GetAll(channelId: channel.ChannelId, limit: EventsPerPage, currentUserId: userid);

            var model = new ChannelViewModel
            {
                Channel = channelDto,
                //Events = eventsDto.ToList(),
                CategoryBroadcasts = BroadcastDto.GetBroadcastsPerCategory(userid, true),
                Users = users,
                Shows = ChannelDto.GetShows(userid, null, TabTypes.All),
                CurrentBroadcast = GroupBroadcast.GetAllByGroupId(channelDto.ChannelId).FirstOrDefault(b => b.Position == 0)
            };


            ViewBag.ChatId = SayYeahTv.Dependencies.Helpers.FormatChatId(channel.ChannelId, null /* privateSessionId*/);
            ViewBag.VideoFramesNumber = Channel.VideoFramesNumber;
            ViewBag.IsOwner = false;
            ViewBag.IsChannelFollower = false;
            ViewBag.IsOwnerFollower = false;

            if (!String.IsNullOrWhiteSpace(userid))
            {
                //var user = _db.Users.FirstOrDefault(x => x.Id == userid);

                //if (user != null)
                //{
                    if (userid == channel.OwnerId)
                        ViewBag.IsOwner = true;

                    var followChannel = _db.Channels.SingleOrDefault(x => x.ChannelId == channel.ChannelId
                                                      && x.Followers.Any(y => y.Id == userid));
                    if (followChannel != null)
                        ViewBag.IsChannelFollower = true;

                    var followOwner = _db.Channels.SingleOrDefault(x => x.ChannelId == channel.ChannelId
                                                      && x.User.Followers.Any( y => y.Id == userid));
                    if (followOwner != null)
                        ViewBag.IsOwnerFollower = true;
                //}
            }

            if (channel.IsPrivate && !ViewBag.IsOwner
                    && _db.Channels.FirstOrDefault(x =>
                                x.ChannelId == channel.ChannelId &&
                                x.ChannelMembers.Any(y => y.UserId == userid)) == null
                )
            {
                return new HttpUnauthorizedResult(); //TODO: show error
            }

            /*
            if (privateSessionId.HasValue)
            {
                var session = _db.PrivateSessions.Include(x => x.Members)
                                 .FirstOrDefault(x => x.ChannelId == channel.ChannelId);

                if(session == null)
                    return HttpNotFound(); //TODO: handle it

                if (User.Identity.IsAuthenticated &&
                    (session.UserId == userid || session.Members.Any(x => x.Id == userid)))
                {
                    model.PrivateSessionId = session.PrivateSessionId;
                }
                else
                {
                    return new HttpUnauthorizedResult(); //TODO: show error
                }
            }*/

            return View(model);
        }

        public ActionResult GetBroadcasts(int channelId, bool isGroup/*, int? privateSessionId = null*/)
        {
            //var isOwner = false;
            if (isGroup)
            {
                /*
                var userid = User.Identity.GetUserId();
                if (!String.IsNullOrWhiteSpace(userid))
                {
                    var channel = Channel.GetByKey(channelId);

                    if (channel != null && channel.OwnerId == userid)
                        isOwner = true;
                }*/

                var obj = new
                {
                    Broadcasts = GroupBroadcast.GetAllByGroupId(channelId/*, privateSessionId*/),
                    BroadcastsNotAccepted = GroupBroadcast.GetNotAcceptedByGroupId(channelId)
                };

                return Json(obj, JsonRequestBehavior.AllowGet); 
            }

            var broadcasts = BroadcastDto.GetAll(channelId);

            return Json(new { Broadcasts = broadcasts}, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult GetEvents(int channelId, int page = 0)
        {
            var events = EventDto.GetAll(channelId: channelId, limit: EventsPerPage, offset: page * EventsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_EventsPartial", events);
        }

        public ActionResult GetRecordings(int channelId, int page = 0, bool presenterOnly = false)
        {
            var recordings = Recording.GetAll(channelId: channelId, limit: RecordingsPerPage, offset: page * RecordingsPerPage, presenterOnly: presenterOnly);
            return PartialView("_RecordingsPartial", Mapper.Map<List<RecordingDto>>(recordings));
        }

        public ActionResult GetFollowers(int channelId, int page = 0)
        {
            var userid = User.Identity.GetUserId();
            var followers = UserDto.GetChannelFollowers(channelId: channelId, limit: FollowersPerPage, offset: page*FollowersPerPage, currentUserId: userid);
            return PartialView("_MembersPartial", Mapper.Map<List<UserDto>>(followers));
        }


        public ActionResult SetShow(int showId = 0)
        {
            var model = new SetShowViewModel
            {
                Categories =  Mapper.Map<IEnumerable<SelectListItem>>(Category.GetAll())
            };

            if (showId > 0)
            {
                // TODO: check if not exists
                var show = _channelManager.GetByKey(showId);
                model.Id = show.ChannelId;
                model.Name = show.Name;
                model.Description = show.Description;

                if (show.Categories != null && show.Categories.Any())
                {
                    var showCategories = show.Categories.ToList();

                    model.Category1 = showCategories[0].CategoryId;

                    if (showCategories.Count > 1)
                        model.Category2 = showCategories[1].CategoryId;

                    if (showCategories.Count > 2)
                        model.Category3 = showCategories[2].CategoryId;
                }
            }

            return PartialView("_SetShowPartial", model);
        }

        //Create new webshow
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetShow(SetShowViewModel show)
        {
            if(!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            Channel channel;

            if (show.Id > 0)
            {
                channel = await _db.Channels.Include(x => x.Categories).FirstOrDefaultAsync(x => x.ChannelId == show.Id);

                if (channel == null)
                    return Json(new { Errors = new []{"Show not found"} }, JsonRequestBehavior.AllowGet);

                if (channel.Name != show.Name)
                {
                    channel.Tag = _namesGenerator.GenerateChannelName(show.Name);
                }
                channel.Name = show.Name;

                foreach (var category in channel.Categories.ToList())
                {
                    channel.Categories.Remove(category);
                }  
            }
            else
            {
                channel = Channel.CreateNewGroup(User.Identity.GetUserId(), show.Name, false);
                channel.Tag = _namesGenerator.GenerateGroupName(show.Name);
                channel.Categories = new Collection<Category>();
                _db.Channels.Add(channel);
            }

            channel.Categories.Add(_db.Categories.Find(show.Category1));

            if (show.Category2 != null)
                channel.Categories.Add(_db.Categories.Find(show.Category2));

            if (show.Category3 != null)
                channel.Categories.Add(_db.Categories.Find(show.Category3));

            if (show.Logo != null)
            {
                var extension = Path.GetExtension(show.Logo.FileName);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    show.Logo.InputStream.CopyTo(memoryStream);
                    var resizedLogo = _imageProcessor.ProcessChannelLogo(memoryStream);
                    channel.Logo = _azureBlobHelper.UploadFile("channellogo",
                        string.Format("{0}{1}", Guid.NewGuid(), extension), resizedLogo);
                    resizedLogo.Dispose();
                }
            }

            if (show.Background != null)
            {
                var extension = Path.GetExtension(show.Background.FileName);
                channel.Background = _azureBlobHelper.UploadFile("channelbackgrounds",
                    string.Format("{0}{1}", Guid.NewGuid(), extension), show.Background.InputStream);
            }

            await _db.SaveChangesAsync();

            var url = Url.RouteUrl("Channel", new {tag = channel.Tag});

            if (show.Id <= 0)
            {
                await _presenterCreatesWebshowMessageManager.SendMessageAsync(new PresenterCreatesWebshowMessage
                {
                    UserId = User.Identity.GetUserId(),
                    ChannelId = channel.ChannelId
                });
            }

            return Json(new { Success = true, Url = url}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveEvent(int eventId)
        {
            var model = new RemoveEventViewModel
            {
                Id = eventId
            };

            return PartialView("_RemoveEventPartial", model);
        }  

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveEvent(RemoveEventViewModel evnt)
        {
            var userId = User.Identity.GetUserId();

            var evt = _db.Events.FirstOrDefault(x => x.EventId == evnt.Id && x.Channel.OwnerId == userId);
            
            if (evt == null)
                return Json(new { Errors = new[] { "Upcoming not found" } }, JsonRequestBehavior.AllowGet);

            _db.Events.Remove(evt);

            _db.SaveChanges();

            return Json(new { Success = true}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveRecording(int recordingId)
        {
            var model = new RemoveRecordingViewModel
            {
                Id = recordingId
            };

            return PartialView("_RemoveRecordingPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveRecording(RemoveRecordingViewModel recording)
        {
            var userId = User.Identity.GetUserId();

            var rec = _db.Recordings.FirstOrDefault(x => x.RecordingId == recording.Id && x.UserId == userId);

            if (rec == null)
                return Json(new { Errors = new[] { "Recording not found" } }, JsonRequestBehavior.AllowGet);

            _db.Recordings.Remove(rec);

            _db.SaveChanges();

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenameRecording(int recordingId)
        {
            var rec = _db.Recordings.FirstOrDefault(x => x.RecordingId == recordingId);

            if (rec == null)
                return Json(new { Errors = new[] { "Recording not found" } }, JsonRequestBehavior.AllowGet);

            var model = new RenameRecordingViewModel
            {
                Id = recordingId,
                Name = rec.Name
            };

            return PartialView("_RenameRecordingPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RenameRecording(RenameRecordingViewModel recording)
        {
            var userId = User.Identity.GetUserId();

            var rec = _db.Recordings.FirstOrDefault(x => x.RecordingId == recording.Id && x.UserId == userId);

            if (rec == null)
                return Json(new { Errors = new[] { "Recording not found" } }, JsonRequestBehavior.AllowGet);

            rec.Name = recording.Name;

            _db.SaveChanges();

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetEvent(int eventId = 0)
        {
            var model = new SetEventViewModel
            {
                Categories = Mapper.Map<IEnumerable<SelectListItem>>(Category.GetAll()),
                Shows = Mapper.Map<IEnumerable<SelectListItem>>(UserDto.GetShows(User.Identity.GetUserId()))
            };

            if (eventId > 0)
            {
                // TODO: check if event not exists
                var ev = _db.Events.SingleOrDefault(x => x.EventId == eventId);
                if (ev != null)
                {
                    model.Id = ev.EventId;
                    model.ShowId = ev.ChannelId;
                    model.Title = ev.Title;
                    model.Description = ev.Description;
                    model.StartDate = StringHelper.FromDateTime(ev.StartDate);
                }
            }

            return PartialView("_SetEventPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetEvent(SetEventViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            Event evt;

            if (model.Id > 0)
            {
                evt = await _db.Events.Include(x => x.Categories).FirstOrDefaultAsync(x => x.EventId == model.Id);

                if (evt == null)
                    return Json(new { Errors = new[] { "Event not found" } }, JsonRequestBehavior.AllowGet);

                foreach (var category in evt.Categories.ToList())
                {
                    evt.Categories.Remove(category);
                }
            }
            else
            {
                evt = new Event
                {
                    Categories = new Collection<Category>(),
                    CreationDate = DateTime.UtcNow
                };
                _db.Events.Add(evt);
            }

            evt.Title = model.Title;
            evt.Description = model.Description;
            evt.ChannelId = model.ShowId;

            DateTime dt;

            if(!StringHelper.ToDateTime(model.StartDate, out dt))
                return Json(new { Errors = new[] { "Start Date is not recognized" } }, JsonRequestBehavior.AllowGet);

            evt.StartDate = dt.AddMinutes(model.TimeZoneOffsetMinutes);

            evt.Categories.Add(_db.Categories.Find(model.Category1));

            if (model.Category2 != null)
                evt.Categories.Add(_db.Categories.Find(model.Category2));

            if (model.Category3 != null)
                evt.Categories.Add(_db.Categories.Find(model.Category3));

            if (model.Logo != null)
            {
                var extension = Path.GetExtension(model.Logo.FileName);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    model.Logo.InputStream.CopyTo(memoryStream);
                    var resizedLogo = _imageProcessor.ProcessChannelLogo(memoryStream);
                    evt.Logo = _azureBlobHelper.UploadFile("channellogo",
                        string.Format("{0}{1}", Guid.NewGuid(), extension), resizedLogo);
                    resizedLogo.Dispose();
                }
            }

            await _db.SaveChangesAsync();

            //var url = Url.RouteUrl("Channel", new { tag = channel.Tag });
            await _webshowSchedulesEventMessageManager.SendMessageAsync(new WebshowSchedulesEventMessage
            {
                EventId = evt.EventId,
                ChannelId = model.ShowId
            });

            await _presenterSchedulesEventMessageManager.SendMessageAsync(new PresenterSchedulesEventMessage
            {
                EventId = evt.EventId,
                UserId = User.Identity.GetUserId()
            });

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetChannelToGroup(int groupId, string userId/*, int? privateSessionId = null*/, int? position = null/*, bool isEmitEvent = true*/)
        {
            if (_channelManager.SetShowMember(groupId, userId, position))
            {
                ChatHubStatic.UpdateShowVideo(groupId);
                //_chatHub.Clients.Group(SayYeahTv.Dependencies.Helpers.FormatChatId(groupId, null /* privateSessionId*/)).updateChannelVideo();
                return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
            }

            throw new HttpException(404, "Not found");
        }


        [HttpPost]
        public async Task<ActionResult> SetChannelMuted(int groupId, string userId/*, int? privateSessionId = null*/, bool isMuted = false)
        {
            var entity = _db.ChannelMembers.FirstOrDefault(x => x.ChannelId == groupId && x.UserId == userId /*&& x.PrivateSessionId == privateSessionId*/);
            if (entity != null)
            {
                entity.IsMuted = isMuted;
            }

            await _db.SaveChangesAsync();

            ChatHubStatic.UpdateShowVideo(groupId);
            //_chatHub.Clients.Group(SayYeahTv.Dependencies.Helpers.FormatChatId(groupId, null /*privateSessionId*/)).updateChannelVideo();

            return Json(new { Status = "OK" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> GetRecordingUrl(int channelId, int recordingId)
        {
            var recording = _db.Recordings.FirstOrDefault(x => x.RecordingId == recordingId && x.ChannelId == channelId);
            if (recording == null)
                throw new HttpException(404, "Not found");

            return Json(Mapper.Map<Recording, RecordingDto>(recording));
        }

        [HttpPost]
        public async Task<ActionResult> GetBroadcastUrl(int channelId, int broadcastId)
        {
            var recording = _db.Broadcasts.FirstOrDefault(x => x.BroadcastId == broadcastId && x.Channels.Any(y => y.ChannelId == channelId) /* ( x.ChannelId == channelId || x.ChannelGroupId == channelId )*/ );
            if (recording == null)
                throw new HttpException(404, "Not found");

            return Json(new { Url = recording.VideoUrl }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Follow(int channelId, bool state)
        {
            var userId = User.Identity.GetUserId();
            var user = _db.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                if (!String.IsNullOrWhiteSpace(userId))
                {
                    var channel = _db.Channels
                        .Include(x => x.Followers)
                        .FirstOrDefault(x => x.ChannelId == channelId);

                    if (channel != null)
                    {
                        var follower = channel.Followers.SingleOrDefault(x => x.Id == userId);

                        if (follower == null)
                        {
                            if (state)
                            {
                                channel.Followers.Add(user);

                                await _gotNewFollowerMessageManager.SendMessageAsync(new GotNewFollowerMessage
                                {
                                    FollowerId = userId,
                                    TargetId = channel.ChannelId.ToString(),
                                    TargetType = GotNewFollowerTargetType.Channel
                                });

                                if (User.IsInAnyRoles(Roles.Presenter))
                                {
                                    await _presenterFollowsWebshowMessageManager.SendMessageAsync(new PresenterFollowsWebshow
                                        {
                                            ChannelId = channel.ChannelId,
                                            UserId = userId
                                        });
                                }
                            }
                        }
                        else
                        {
                            if (!state)
                            {
                                channel.Followers.Remove(follower);
                            }
                        }


                        await _db.SaveChangesAsync();
                        return Json(new {Status = "OK"}, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json(new { Status = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetBroadcast(Guid uid, bool withRecord = false)
        {
            var broadcast = _db.Broadcasts.FirstOrDefault(x => x.Uid == uid);

            if (broadcast == null)
                throw new HttpException(404, "Not found");

            var model = new SetBroadcastViewModel
            {
                Categories = Mapper.Map<IEnumerable<SelectListItem>>(Category.GetAll()),
                Id = broadcast.BroadcastId,
                WithRecord = withRecord,
                //Name = broadcast.Name,
                //Description = broadcast.Description
            };

            if (broadcast.Channel.Categories != null && broadcast.Channel.Categories.Any())
            {
                var broadcastCategories = broadcast.Channel.Categories.ToList();

                model.Category1 = broadcastCategories[0].CategoryId;

                if (broadcastCategories.Count > 1)
                    model.Category2 = broadcastCategories[1].CategoryId;

                if (broadcastCategories.Count > 2)
                    model.Category3 = broadcastCategories[2].CategoryId;
            }

            return PartialView("_SetBroadcastPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetBroadcast(SetBroadcastViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            var broadcast = await _db.Broadcasts.Include(x => x.Categories).FirstOrDefaultAsync(x => x.BroadcastId == model.Id);

            if (broadcast == null)
                return Json(new { Errors = new[] { "Broadcast not found" } }, JsonRequestBehavior.AllowGet);

            foreach (var category in broadcast.Categories.ToList())
            {
                broadcast.Categories.Remove(category);
            }

            broadcast.Name = model.Name;
            broadcast.Description = model.Description;
            broadcast.WithRecord = model.WithRecord;

            if (model.GoLiveType == GoLiveTypes.Encoder)
                broadcast.Type = BroadcastTypes.Encoder;

            broadcast.Categories.Add(_db.Categories.Find(model.Category1));

            if (model.Category2 != null)
                broadcast.Categories.Add(_db.Categories.Find(model.Category2));

            if (model.Category3 != null)
                broadcast.Categories.Add(_db.Categories.Find(model.Category3));

            await _db.SaveChangesAsync();

            return Json(new
            {
                Success = true,
                Data = new
                {
                    broadcast.Uid, //need only for wowza simulation
                    FileName = broadcast.GetShortPublishingUrl(),
                    //FileName = broadcast.GetShortUrl(),
                    PublishingUrl = broadcast.GetPublishingUrl(),
                    Type = model.GoLiveType.ToString(),
                    IsRecord = broadcast.WithRecord
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreatePrivateSession(int channelId)
        {
            var userid = User.Identity.GetUserId();
            if (String.IsNullOrWhiteSpace(userid))
                return RedirectToAction("Login", "Account");

            var model = new SetPrivateShowViewModel
            {
                ChannelId = channelId,
                Candidates = UserDto.GetAll()
            };

            return PartialView("_SetPrivateSessionPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePrivateSession(SetPrivateShowViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpUnauthorizedResult();

            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            var channel = _db.Channels.FirstOrDefault(x => x.ChannelId == model.ChannelId);

            if (channel == null)
                return Json(new { Errors = new[] { "Show not found" } }, JsonRequestBehavior.AllowGet);

            var userId = User.Identity.GetUserId();
            var user = _db.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
                return new HttpUnauthorizedResult();

            var userChannel = _db.Channels.FirstOrDefault(x => x.OwnerId == user.Id && x.Type == ChannelTypes.ChannelGroup && x.IsPrivate);

            if (userChannel == null)
            {
                userChannel = Channel.CreateNewGroup(user.Id, user.Email);
                userChannel.IsPrivate = true;
                userChannel.Tag = _namesGenerator.GenerateChannelName(user.Email);
                userChannel.ChannelMembers = new Collection<ChannelMember>();
                userChannel.BroadcastsToGroup = new Collection<Broadcast>();

                _db.Channels.Add(userChannel);
            }
            else
            {
                if (userChannel.ChannelMembers != null)
                {
                    foreach (var memb in _db.ChannelMembers.Where(x => x.ChannelId == userChannel.ChannelId))
                    {
                        userChannel.ChannelMembers.Remove(memb);
                    }
                }
            }

            _db.SaveChanges();

            if (userChannel.ChannelMembers == null)
                userChannel.ChannelMembers = new Collection<ChannelMember>();

            if (model.CandidatesAccepted != null)
            {
                foreach (var member in model.CandidatesAccepted)
                {
                    _channelManager.SetShowMember(userChannel.ChannelId, member, null);

                    ChatHubStatic.UpdateShowVideo(userChannel.ChannelId);
                    //_chatHub.Clients.Group(SayYeahTv.Dependencies.Helpers.FormatChatId(userChannel.ChannelId, null /* privateSessionId*/)).updateChannelVideo();
                }
            }

            var channelMember = channel.ChannelMembers.FirstOrDefault(x => x.IsAccepted && x.IsPositionAccepted && x.Position == 0);

            if (channelMember != null)
            {
                _channelManager.SetShowMember(userChannel.ChannelId, channelMember.UserId, 0);
                ChatHubStatic.UpdateShowVideo(userChannel.ChannelId);
                //_chatHub.Clients.Group(SayYeahTv.Dependencies.Helpers.FormatChatId(userChannel.ChannelId, null /* privateSessionId*/)).updateChannelVideo();

                var broascast = _db.Broadcasts.OrderByDescending(x => x.BroadcastId)
                                .FirstOrDefault(x => x.Channels.Any(y => y.ChannelId == channel.ChannelId && x.IsOnAir));

                if (broascast != null)
                {
                    userChannel.BroadcastsToGroup.Add(broascast);
                }
            }

            _db.SaveChanges();

            var url = Url.RouteUrl("Channel", new { tag = userChannel.Tag});

            return Json(new { Success = true, Url = url }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateBroadcast(int channelId, /*int? privateSessionId = null, bool isGroup = false, int? position = null,*/ string geolocation = null, bool isAutoName = false)
        {
            //TODO: log and send all errors to user!
            var userid = User.Identity.GetUserId();
            if (String.IsNullOrWhiteSpace(userid))
                return new HttpUnauthorizedResult();

            var user = _db.Users.FirstOrDefault(x => x.Id == userid);
            if (user == null)
                return new HttpUnauthorizedResult();

            var group = _db.Channels.FirstOrDefault(x => x.ChannelId == channelId);
            if (group == null)
                throw new ObjectNotFoundException("Show not found"); //TODO: handle it

            var channel = user.Channels.FirstOrDefault(x => x.Type == ChannelTypes.Channel && !x.IsPrivate); //TODO: get default channel

            if (channel == null)
            {
                channel = Channel.CreateNewChannel(user.Id, user.Email);
                channel.Tag = _namesGenerator.GenerateChannelName(user.Email);
                _db.Channels.Add(channel);
                //_dbContext.Channels.Add(newGroup);
                _db.SaveChanges();
            }

            var mediaServerId = MediaServer.GetIdToBroadcast();

            var broadcast = new Broadcast
            {
                UserId = user.Id,
                Uid = Guid.NewGuid(),
                CreationDate = DateTime.UtcNow,
                IsOnAir = false,
                //State = onAir ? States.Active : States.New,
                Type = BroadcastTypes.WebCamera,
                MediaServerId = mediaServerId,
                MediaServer = _db.MediaServers.Find(mediaServerId),
                ChannelId = channel.ChannelId,
                ChannelGroupId = group.ChannelId,
                Channels = new Collection<Channel>{ channel, group }
            };

            if (!String.IsNullOrWhiteSpace(geolocation))
                broadcast.Location = DbGeography.FromText("POINT(" + geolocation + ")");

             if (isAutoName)
                 broadcast.Name = user.UserName + " Broadcast";     
  
            _db.Broadcasts.Add(broadcast);
            _db.SaveChanges();

            return Json(new
            {
                broadcast.Uid, //need only for wowza simulation
                FileName = broadcast.GetShortPublishingUrl(),
                //FileName = broadcast.GetShortUrl(),
                PublishingUrl = broadcast.GetPublishingUrl()
            }, JsonRequestBehavior.AllowGet);

            /*
            if (isGroup)
            {
                if (channelId == null)
                    throw new ArgumentNullException("channelId", "Group ID is empty");


                var group = Channel.GetByKey(channelId);

                if(group == null)
                    throw new ObjectNotFoundException("Group not found");

                broadcast.ChannelGroupId = group.ChannelId;
                if (group.Categories != null)
                {
                    broadcast.Categories = new Collection<Category>();
                    foreach (var cat in group.Categories)
                    {
                        broadcast.Categories.Add(_db.Categories.FirstOrDefault(x => x.CategoryId == cat.CategoryId));
                    }
                }

                channelId = null;
            }

            var channel = channelId == null
                        ? user.Channels.FirstOrDefault(x => x.Type == ChannelTypes.Channel) //TODO: get default channel
                        : user.Channels.FirstOrDefault(x => x.ChannelId == channelId);
    
            if (channel != null && channel.OwnerId == user.Id && channel.Type == ChannelTypes.Channel)
            {
                broadcast.ChannelId = channel.ChannelId;

                if (isAutoName)
                    broadcast.Name = user.UserName + " Broadcast";

                if (broadcast.Categories == null && channel.Categories != null)
                {
                    broadcast.Categories = new Collection<Category>();
                    foreach (var cat in channel.Categories)
                    {
                        broadcast.Categories.Add(_db.Categories.FirstOrDefault(x => x.CategoryId == cat.CategoryId));
                    }
                }

                _db.Broadcasts.Add(broadcast);
                _db.SaveChanges();

                if (position.HasValue && broadcast.ChannelGroupId.HasValue)
                    await SetChannelToGroup((int)broadcast.ChannelGroupId, broadcast.ChannelId, privateSessionId, position, false);

                return Json(new
                {
                    broadcast.Uid, //need only for wowza simulation
                    FileName = broadcast.GetShortUrl(),
                    PublishingUrl = broadcast.GetPublishingUrl()
                }, JsonRequestBehavior.AllowGet);
            }
            */
        }

        public async Task<ActionResult> PresenterIsOnWebshow(int channelId)
        {
            await _presenterJoinsWebshowLongMessageManager.SendMessageAsync(new PresenterJoinsWebshowLong
            {
                ChannelId = channelId,
                UserId = User.Identity.GetUserId()
            });

            return Json(true);
        }

        public ActionResult DeleteChannel(int id)
        {
            var model = new RemoveEventViewModel
            {
                Id = id
            };

            return PartialView("_RemoveChannelPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteChannel(RemoveChannelViewModel model)
        {
            await _channelManager.DeleteAsync(model.Id, User);
            return Json(true);
        }
    }
}