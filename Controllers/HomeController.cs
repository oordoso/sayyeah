﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using SayYeahTv.Dependencies.Communications;
using SayYeahTv.Dependencies.Communications.Messages;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Extensions;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Management.Interfaces;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;
using SayYeahTV.Web.Models.Enums;

namespace SayYeahTV.Web.Controllers
{
    public class HomeController : Controller
    {
        private const int UsersPerPage = 24;
        private const int EventsPerPage = 18;

        private readonly SayYeahTVContext _db;
        private readonly IMessageQueueManager<GotNewFollowerMessage> _gotNewFollowerMessageManager;
        private readonly IMessageQueueManager<PresenterFollowsPresenter> _presenterFollowPresenterMessageManager;
        private readonly IMessageQueueManager<PresenterSubscribesEvent> _presenterSubscribesEventMessageManager;
        private readonly IChannelManager _channelManager;

        public HomeController(SayYeahTVContext db,
            IMessageQueueManager<GotNewFollowerMessage> gotNewFollowerMessageManager,
            IMessageQueueManager<PresenterFollowsPresenter> presenterFollowPresenterMessageManager,
            IMessageQueueManager<PresenterSubscribesEvent> presenterSubscribesEventMessageManager,
            IChannelManager channelManager)
        {
            _db = db;
            _gotNewFollowerMessageManager = gotNewFollowerMessageManager;
            _presenterFollowPresenterMessageManager = presenterFollowPresenterMessageManager;
            _presenterSubscribesEventMessageManager = presenterSubscribesEventMessageManager;
            _channelManager = channelManager;
        }

        public ActionResult Index()
        {
            var model = new HomeViewModel();

            var skipLandingCookie = Request.GetOwinContext().Request.Cookies["skiplanding"];
            model.SkipLanding = User.Identity.IsAuthenticated || skipLandingCookie != null && skipLandingCookie == "1";

            int showId = 0;
            var userid = User.Identity.GetUserId();
            var users = UserDto.GetAll(limit: UsersPerPage, currentUserId: userid, type: TabTypes.All).ToList();
            model.Users = users;
            model.Shows = ChannelDto.GetShows(userid, null, TabTypes.All);
            model.CategoryBroadcasts = BroadcastDto.GetBroadcastsPerCategory(userid, true);

            ViewBag.VideoFramesNumber = Channel.VideoFramesNumber;
            ViewBag.IsOwner = false;
            ViewBag.IsChannelFollower = false;
            ViewBag.IsOwnerFollower = false;
            ViewBag.ChatId = null;

            //if (int.TryParse(SystemParameter.GetValueByTag("HomePageShowId"), out showId))

            //Get the latest LIVE channel as placeholder
            var latestBroadcast = BroadcastDto.GetLiveBroadcasts(limit: 1).FirstOrDefault();

            if (latestBroadcast != null && latestBroadcast.ChannelGroupId != null)
            {
                showId = (int)latestBroadcast.ChannelGroupId;
            }
            else //overwise get the channel with latest recording
            {
                var recording = _db.Recordings.OrderByDescending(x => x.RecordingId).FirstOrDefault(
                        x => x.State == States.Active && x.Channel.State == States.Active && x.Channel.Type == ChannelTypes.ChannelGroup
                    );

                if (recording != null)
                {
                    showId = recording.ChannelId;
                }
            }


            if(showId > 0)
            {
                var show = _channelManager.GetByKey(showId);

                model.Show = Mapper.Map<ChannelDto>(show);

                if (!String.IsNullOrWhiteSpace(userid))
                {
                    if (userid == show.OwnerId)
                        ViewBag.IsOwner = true;

                    var followChannel = _db.Channels.SingleOrDefault(x => x.ChannelId == show.ChannelId
                                                        && x.Followers.Any(y => y.Id == userid));
                    if (followChannel != null)
                        ViewBag.IsChannelFollower = true;

                    var followOwner = _db.Channels.SingleOrDefault(x => x.ChannelId == show.ChannelId
                                                        && x.User.Followers.Any(y => y.Id == userid));
                    if (followOwner != null)
                        ViewBag.IsOwnerFollower = true;
                }

                ViewBag.ChatId = SayYeahTv.Dependencies.Helpers.FormatChatId(model.Show.ChannelId, null /* privateSessionId*/);
            }

            //var userId = User.Identity.GetUserId();
            //await DocumentDBRepository.AddUserNotificationAsync(userId, "Test 1");
            //var nots = DocumentDBRepository.GetUserNotifications(userId);
            return View(model);
        }

        public ActionResult WatchVideos()
        {
            Request.GetOwinContext().Response.Cookies.Append("skiplanding", "1");
            return RedirectToAction("Index");
        }

        /*
        public ActionResult PlayerTest()
        {
            return View();
        }*/

        public ActionResult Ping()
        {
            try
            {
                var list = _db.Logs.Any();
            }
            catch (Exception)
            {                
                return new HttpStatusCodeResult(500, "DB Error");
            }

            return new HttpStatusCodeResult(200);
        }

        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> GetNotifications()
        {
            var userId = User.Identity.GetUserId();
            var notifications = _db.Notifications.Where(x => x.UserId == userId && x.IsVisible)
                .OrderByDescending(x => x.Id)
                .Take(10)
                .ToList();

            if (notifications.Any())
            {
                var currentUser = _db.Users.Find(userId);
                var model = new NotificationsListViewModel
                {
                    Notifications = Mapper.Map<IEnumerable<Notification>, IEnumerable<NotificationDto>>(notifications,
                        options =>
                        {
                            options.Items["db"] = _db;
                            options.Items["currentUser"] = currentUser;
                        })
                };

                foreach (var notification in notifications)
                {
                    notification.IsVisible = false;
                }

                await _db.SaveChangesAsync();

                return PartialView("_Notifications", model);
            }
            return PartialView("_NotificationsEmpty");
        }

        public ActionResult Upcomings()
        {
            ViewBag.EventsLimit = EventsPerPage;
            var events = EventDto.GetAll(limit: EventsPerPage, currentUserId: User.Identity.GetUserId());
            return View(events);
        }

        public ActionResult GetStreamers(TabTypes type)
        {
            var shows = ChannelDto.GetShows(User.Identity.GetUserId(), null, type);
            return PartialView("_StreamersHostPartial", shows);
        }

        public ActionResult GetPeople(TabTypes type)
        {
            var people = UserDto.GetAll(limit: UsersPerPage, currentUserId: User.Identity.GetUserId(), type: type).ToList();
            return PartialView("_UsersHomePartial", people);
        }
        

        public ActionResult GetLiveBroadcasts()
        {
            var broadcasts = BroadcastDto.GetLiveBroadcasts(User.Identity.GetUserId(), true);
            return PartialView("_BroadcastsPartial", broadcasts);
        }

        public ActionResult GetEvents(int page = 0)
        {
            var events = EventDto.GetAll(limit: EventsPerPage, offset: page * EventsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_EventsPartial", events);
        }

        public ActionResult GetAllEvents(int page = 0)
        {
            var events = EventDto.GetAll(limit: EventsPerPage, offset: page * EventsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_EventsPartial", events);
        }

        public ActionResult GetAllUsers(int page = 0)
        {
            var users = UserDto.GetAll(limit: UsersPerPage, offset: page * UsersPerPage, currentUserId: User.Identity.GetUserId()).ToList();
            return PartialView("_UsersPartial", users);
        }

        public ActionResult GetTop(int page = 0)
        {
            var channels = ChannelDto.GetShows(User.Identity.GetUserId(), null, type: TabTypes.Top);
            return PartialView("_ChannelsPartial", channels);
        }

        public ActionResult GetRecent(int page = 0)
        {
            var recordings = Recording.GetAll(limit: UsersPerPage, offset: page * EventsPerPage, presenterOnly:true);//await _recordingManager.SearchAsync(term, 4);
            return PartialView("_RecordingsPartial", Mapper.Map<IEnumerable<RecordingDto>>(recordings));
        }

        [HttpPost]
        public async Task<ActionResult> Follow(string followId, bool state)
        {
            var userId = User.Identity.GetUserId();
            var user = _db.Users.FirstOrDefault(x => x.Id == userId);

            if (user != null)
            {
                if (!String.IsNullOrWhiteSpace(userId))
                {
                    var userToFollow = _db.Users
                        .Include(x => x.Followers)
                        .FirstOrDefault(x => x.Id == followId);

                    if (userToFollow != null)
                    {
                        var follower = userToFollow.Followers.SingleOrDefault(x => x.Id == userId);

                        if (follower == null)
                        {
                            if (state)
                            {
                                userToFollow.Followers.Add(user);
                                await _gotNewFollowerMessageManager.SendMessageAsync(new GotNewFollowerMessage
                                {
                                    TargetId = userToFollow.Id,
                                    FollowerId = userId,
                                    TargetType = GotNewFollowerTargetType.User
                                });

                                if (User.IsInAnyRoles(Roles.Presenter))
                                {
                                    await _presenterFollowPresenterMessageManager.SendMessageAsync(new PresenterFollowsPresenter
                                    {
                                        FollowerId = User.Identity.GetUserId(),
                                        FollowedId = userToFollow.Id
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (!state)
                            {
                                userToFollow.Followers.Remove(follower);
                            }
                        }


                        await _db.SaveChangesAsync();
                        return Json(new { Status = "OK" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { Status = "Error" }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        public async Task<ActionResult> Notify(int eventId, bool state)
        {
            var userId = User.Identity.GetUserId();
            var user = _db.Users.FirstOrDefault(x => x.Id == userId);

            if (user != null)
            {
                if (!String.IsNullOrWhiteSpace(userId))
                {
                    var evt = _db.Events
                                .Include(x => x.Users)
                                .FirstOrDefault(x => x.EventId == eventId);

                    if (evt != null)
                    {
                        var obj = evt.Users.SingleOrDefault(x => x.Id == userId);

                        if (obj == null)
                        {
                            if (state)
                            {
                                evt.Users.Add(user);

                                if (User.IsInAnyRoles(Roles.Presenter))
                                {
                                    await _presenterSubscribesEventMessageManager.SendMessageAsync(new PresenterSubscribesEvent
                                    {
                                        EventId = eventId,
                                        UserId = userId
                                    });
                                }
                            }
                        }
                        else
                        {
                            if (!state)
                            {
                                evt.Users.Remove(obj);
                            }
                        }


                        await _db.SaveChangesAsync();
                        return Json(new { Status = "OK" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { Status = "Error" }, JsonRequestBehavior.AllowGet);
        }
    }
}