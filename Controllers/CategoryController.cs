﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;
using SayYeahTV.Web.Models.Enums;
using WebGrease.Css.Extensions;

namespace SayYeahTV.Web.Controllers
{
    public class CategoryController : Controller
    {
        private const int EventsPerPage = 16;
        private const int RecordingsPerPage = 16;
        private const int UsersPerPage = 16;
        private const int BroadcastsPerPage = 16;

        private readonly SayYeahTVContext _db;

        public CategoryController(SayYeahTVContext db)
        {
            _db = db;
        }

        public ActionResult Index(string tag = null)
        {
            var model = new Dictionary<CategoryDto, IEnumerable<BroadcastDto>>();
            List<Category> categories;
            if (String.IsNullOrWhiteSpace(tag))
            {
                categories = Category.GetAll();
            }
            else
            {
                var cat = _db.Categories.FirstOrDefault(x => x.Name == tag);

                if (cat == null)
                    throw new HttpException(404, "Not found");

                categories = new List<Category> {cat};
            }

            foreach (var category in categories)
            {
                var broadcasts = BroadcastDto.GetAll(categoryId: category.CategoryId, limit: BroadcastsPerPage, currentUserId: User.Identity.GetUserId());
                model.Add(Mapper.Map<Category, CategoryDto>(category), broadcasts);
            }

            ViewBag.EventsLimit = EventsPerPage;
            ViewBag.RecordingsLimit = RecordingsPerPage;
            ViewBag.FollowersLimit = UsersPerPage;
            ViewBag.BroadcastsLimit = BroadcastsPerPage;

            return View(model);
        }

        public ActionResult LiveNow()
        {
            var userid = User.Identity.GetUserId();
            var model = BroadcastDto.GetLiveBroadcasts(userid, true);
            return View(model);
        }

        public ActionResult WebShows()
        {
            var userid = User.Identity.GetUserId();
            var model = ChannelDto.GetShows(userid, null, TabTypes.Alphabet);
            return View(model);
        }

        public ActionResult GetBroadcasts(int id, int page = 0)
        {
            var broadcasts = BroadcastDto.GetAll(categoryId: id, limit: BroadcastsPerPage, offset: page * BroadcastsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_BroadcastsPartial", broadcasts);
        }

        public ActionResult GetUpcomings(int id, int page = 0)
        {
            var events = EventDto.GetAll(categoryId: id, limit: EventsPerPage, offset: page * EventsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_EventsPartial", events);
        }

        public ActionResult GetRecent(int id, int page = 0)
        {
            var recordings = Recording.GetAll(categoryId: id, limit: RecordingsPerPage, offset: page * RecordingsPerPage);
            return PartialView("_RecordingsPartial", Mapper.Map<List<RecordingDto>>(recordings));
        }

        public ActionResult GetUsers(int id, int page = 0)
        {
            var users = UserDto.GetAll(categoryId: id, limit: UsersPerPage, offset: page * UsersPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_UsersFiveInRow", users);
        }
    }
}