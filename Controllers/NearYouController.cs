﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web.Mvc;
using SayYeahTV.DataAccess;
using System.Data.Entity;

namespace SayYeahTV.Web.Controllers
{
    public class NearYouController : Controller
    {
        private readonly SayYeahTVContext _db;

        public NearYouController(SayYeahTVContext db)
        {
            _db = db;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetBroadcasts(string geolocation = null, int? radius = null)
        {
            var list = new List<object>();
            var broadcasts = _db.Broadcasts.Include(x => x.Channel)
                                .Where(x => x.IsOnAir && x.Location != null);

            if (!String.IsNullOrWhiteSpace(geolocation) && radius.HasValue)
            {
                broadcasts = broadcasts.Where(
                        x => x.Location.Distance(DbGeography.FromText("POINT(" + geolocation + ")")) <= radius * 1000
                    );
            }

            broadcasts = broadcasts.OrderByDescending(x => x.BroadcastId)
                                    .Take(200);

            foreach (var broadcast in broadcasts)
            {
                var location = broadcast.Location;

                list.Add(new
                {
                    broadcast.Name,
                    Url = String.Format(
                        "{0}#broadcast={1}", 
                        Url.RouteUrl("Channel", new {tag = broadcast.Channel.Tag}),
                        broadcast.BroadcastId
                    ),
                    location.Latitude,
                    location.Longitude
                });
            }

            return Json(list);
        }

    }
}