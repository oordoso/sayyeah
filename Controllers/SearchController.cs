﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Management.Interfaces;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;
using SayYeahTV.Web.Models.Enums;

namespace SayYeahTV.Web.Controllers
{
    public class SearchController : Controller
    {
        #region  Fields

        //private readonly SayYeahTVContext _db;
        //private readonly IEventManager _eventManager;
        //private readonly IRecordingManager _recordingManager;
        //private readonly IUserManager _userManager;

        private const int ItemsPerPage = 12;

        #endregion

        #region Constructors

        /*
        public SearchController(SayYeahTVContext db, IEventManager eventManager, IRecordingManager recordingManager,
            IUserManager userManager)
        {
            _db = db;
            _eventManager = eventManager;
            _recordingManager = recordingManager;
            _userManager = userManager;
        }*/

        #endregion

        #region Methods

        public ActionResult Index(string term = null)
        {
            var model = new SearchResultViewModel
            {
                Term = term,
                Broadcasts = BroadcastDto.GetAll(term: term, limit: ItemsPerPage, currentUserId: User.Identity.GetUserId())
            };

            ViewBag.ItemsLimit = ItemsPerPage;
            return View(model);
        }

        public async Task<ActionResult> GetLive(string term, int page = 0)
        {
            var broadcasts = BroadcastDto.GetAll(term: term, limit: ItemsPerPage, offset: page * ItemsPerPage, currentUserId: User.Identity.GetUserId());
            return PartialView("_BroadcastsPartial", broadcasts);
        }

        public async Task<ActionResult> GetTop(string term, int page = 0)
        {
            var channels = ChannelDto.GetShows(term: term, currentUserId: User.Identity.GetUserId(), type: TabTypes.Top);
            return PartialView("_ChannelsPartial", channels);
        }

        public async Task<ActionResult> GetRecent(string term, int page = 0)
        {
            var recordings = Recording.GetAll(term: term, limit: ItemsPerPage, offset: page * ItemsPerPage);//await _recordingManager.SearchAsync(term, 4);
            return PartialView("_RecordingsPartial", Mapper.Map<IEnumerable<RecordingDto>>(recordings));
        }

        public async Task<ActionResult> GetUpcomings(string term, int page = 0)
        {
            var events = EventDto.GetAll(term: term, limit: ItemsPerPage, offset: page * ItemsPerPage, currentUserId: User.Identity.GetUserId());//await _eventManager.SearchAsync(term, 4);
            return PartialView("_EventsPartial", events);
        }

        public async Task<PartialViewResult> GetUsers(string term, int page = 0)
        {
            var users = UserDto.GetAll(term: term, limit: ItemsPerPage, offset: page * ItemsPerPage, currentUserId: User.Identity.GetUserId());//await _userManager.SearchAsync(term, 4);
            return PartialView("~/Views/Home/_UsersHomePartial.cshtml", users.ToList());
        }

        #endregion
    }
}