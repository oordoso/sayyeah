﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using SayYeahTv.Dependencies.Communications.Interfaces;
using SayYeahTv.Dependencies.Services.Interfaces;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.OAuth;

namespace SayYeahTV.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly SayYeahTVContext _dbContext;
        private readonly IAzureBlobHelper _azureBlobHelper;
        private readonly INamesGenerator _namesGenerator;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IEmailSender _emailSender;
        private readonly IImageProcessor _imageProcessor;

        public AccountController(SayYeahTVContext dbContext, IAzureBlobHelper azureBlobHelper, 
            INamesGenerator namesGenerator, IEmailSender emailSender, IImageProcessor imageProcessor)
        {
            _dbContext = dbContext;
            _azureBlobHelper = azureBlobHelper;
            _namesGenerator = namesGenerator;
            _emailSender = emailSender;
            _imageProcessor = imageProcessor;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult PreLogin(bool isSignUp = false)
        {
            //ViewBag.ReturnUrl = returnUrl;
            ViewBag.IsSignUp = isSignUp;
            return PartialView("_LoginPartial");
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            //ViewBag.ReturnUrl = returnUrl;
            return PartialView("_LoginPartial");
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Errors = ModelState.Errors()}, JsonRequestBehavior.AllowGet);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var user =
                _dbContext.Users.FirstOrDefault(
                    u => u.UserName == model.EmailOrUsername || u.Email == model.EmailOrUsername);
            if (user != null)
            {
                if (user.State == States.Deleted)
                {
                    return Json(new {Errors = new[] {"Sorry, but your account was deleted."}});
                }

                model.EmailOrUsername = user.UserName;
            }

            // Require the user to have a confirmed email before they can log on.
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    return Json(new { Errors = new[] { "You must have a confirmed email to log on." } }, JsonRequestBehavior.AllowGet);
                }
            }

            var result = await SignInManager.PasswordSignInAsync(model.EmailOrUsername, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                case SignInStatus.LockedOut:
                    //return View("Lockout");
                case SignInStatus.RequiresVerification:
                    //return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    //ModelState.AddModelError("", "Invalid login attempt.");
                    return Json(new { Errors = new []{"Invalid login attempt."}}, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                throw new HttpException(500, "Internal server error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return PartialView("_RegisterPartial");
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RegisterEmailCinfirmation()
        {
            return PartialView("_RegisterEmailCinfirmationPartial");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Errors = ModelState.Errors()}, JsonRequestBehavior.AllowGet);
            }

            var user = new User
            {
                UserName = model.UserName, //_namesGenerator.GenerateUserName(model.Email),
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                RegisterDateTime = DateTime.UtcNow
            };

            if (Request.Cookies != null && Request.Cookies["referrer"] != null)
            {
                user.Referrer = Request.Cookies["referrer"].Value;
            }
           
            var result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                return Json(new { Errors = result.Errors }, JsonRequestBehavior.AllowGet);

            //  Comment the following line to prevent log in until the user is confirmed.
            await UserManager.AddToRoleAsync(user.Id, Enum.GetName( typeof(DataAccess.Enums.Roles), DataAccess.Enums.Roles.User));

            //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                    
            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            await _emailSender.SendConfirmationEmailAsync(model.Email, callbackUrl);

            //await AddUserDefaultChannelsToUser(user);
            await AddUserDefaultNotificationSettings(user);
            //return RedirectToAction("Index", "Home");
            //return Json(new { Errors = new List<string>(){"Please confirm your email."} }, JsonRequestBehavior.AllowGet);
            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);

            
            //AddErrors(result);
            //return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                throw new HttpException((int) HttpStatusCode.BadRequest, "Bad request");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            throw new HttpException((int)HttpStatusCode.InternalServerError, "Bad request");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return PartialView("_ForgotPasswordPartial");
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null /*|| !(await UserManager.IsEmailConfirmedAsync(user.Id))*/)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //return View("ForgotPasswordConfirmation");
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                code = Uri.EscapeDataString(code);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //return RedirectToAction("ForgotPasswordConfirmation", "Account");
                await _emailSender.SendMessageAsync(model.Email, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }

            // If we got this far, something failed, redisplay form
            return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);
            //return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            if (code == null)
            {
                throw new HttpException((int) HttpStatusCode.BadRequest, "Bad request");
            }

            return View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var code = Uri.UnescapeDataString(model.Code);
            var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                throw new HttpException(404, "User not found");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                throw new HttpException(500, "Internal server error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return View("ExternalLoginFailure");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new {ReturnUrl = returnUrl, RememberMe = false});
                case SignInStatus.Failure:                    
                default:

                    // Add account to an existing user
                    var oldUser = await _dbContext.Users.SingleOrDefaultAsync(u => u.Email == loginInfo.Email);
                    if (oldUser != null)
                    {
                        if (oldUser.State == States.Deleted)
                        {
                            // TODO: Show that account was deleted instead
                            return View("ExternalLoginFailure");
                        }

                        await UpdateUserInformation(loginInfo, oldUser);
                        await _dbContext.SaveChangesAsync();
                        await UserManager.AddLoginAsync(oldUser.Id, loginInfo.Login);
                        await SignInManager.SignInAsync(oldUser, false, false);
                        return RedirectToLocal(returnUrl);
                    }

                    var user = new User
                    {
                        UserName = _namesGenerator.GenerateUserName(loginInfo.Email), 
                        Email = loginInfo.Email, 
                        RegisterDateTime = DateTime.UtcNow,
                        EmailConfirmed = true
                    };

                    if (Request.Cookies != null && Request.Cookies["referrer"] != null)
                    {
                        user.Referrer = Request.Cookies["referrer"].Value;
                    }

                    var socialNetworkToken = await UpdateUserInformation(loginInfo, user);

                    var userCreationResult = await UserManager.CreateAsync(user);
                    if (userCreationResult.Succeeded)
                    {
                        await UserManager.AddClaimAsync(user.Id, socialNetworkToken);
                        await UserManager.AddToRoleAsync(user.Id, Enum.GetName(typeof(DataAccess.Enums.Roles), DataAccess.Enums.Roles.User));
                        userCreationResult = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                        if (userCreationResult.Succeeded)
                        {
                            //await AddUserDefaultChannelsToUser(user);
                            await AddUserDefaultNotificationSettings(user);
                            await SignInManager.SignInAsync(user, false, false);
                            return RedirectToLocal(returnUrl);
                        }
                    }

                    return View("ExternalLoginFailure");
            }
        }

        private async Task<Claim> UpdateUserInformation(ExternalLoginInfo loginInfo, User user)
        {
            Claim socialNetworkToken = await GetSocialNetworkToken(loginInfo.Login.LoginProvider);
            string photoFile = string.Format("{0}.jpg", Guid.NewGuid().ToString("N"));

            if (socialNetworkToken != null)
            {
                GenderType genderType;
                switch (loginInfo.Login.LoginProvider)
                {
                    case "Google":
                        using (var webClient = new WebClient())
                        {
                            var jsonResponse =
                                webClient.DownloadString(
                                    new Uri(
                                        string.Format(
                                            "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={0}",
                                            socialNetworkToken.Value)));
                            var googlePlusInfo = JsonConvert.DeserializeObject<GooglePlusUserInfo>(jsonResponse);

                            if (string.IsNullOrEmpty(user.FirstName))
                            {
                                user.FirstName = googlePlusInfo.GivenName;
                            }

                            if (string.IsNullOrEmpty(user.LastName))
                            {
                                user.LastName = googlePlusInfo.FamilyName;
                            }
                            if (user.Gender == GenderType.SayGender)
                            {
                                if (Enum.TryParse(googlePlusInfo.Gender, true, out genderType))
                                {
                                    user.Gender = genderType;
                                }
                            }

                            if (string.IsNullOrEmpty(user.Photo))
                            {
                                using (var photoStream = new MemoryStream(webClient.DownloadData(googlePlusInfo.Picture)))
                                {
                                    using (var userAvatar = _imageProcessor.ProcessUserAvatar(photoStream))
                                    {
                                        user.Photo = _azureBlobHelper.UploadFile("userphoto", photoFile, userAvatar);
                                    }
                                }
                            }
                        }
                        break;

                    case "Facebook":
                        var facebookClient = new FacebookClient(socialNetworkToken.Value);
                        dynamic info = facebookClient.Get("/me");
                        if (string.IsNullOrEmpty(user.FirstName))
                        {
                            user.FirstName = info.first_name;
                        }

                        if (string.IsNullOrEmpty(user.LastName))
                        {
                            user.LastName = info.last_name;
                        }

                        if (user.Gender == GenderType.SayGender)
                        {
                            if (Enum.TryParse(info.gender, true, out genderType))
                            {
                                user.Gender = genderType;
                            }
                        }

                        if (string.IsNullOrEmpty(user.Photo))
                        {
                            try
                            {
                                using (WebClient webClient = new WebClient())
                                {
                                    using (
                                        var photoStream =
                                            new MemoryStream(
                                                webClient.DownloadData(
                                                    string.Format("https://graph.facebook.com/{0}/picture?type=large",
                                                        info.id))))
                                    {
                                        using (var userAvatar = _imageProcessor.ProcessUserAvatar(photoStream))
                                        {
                                            userAvatar.Position = 0;
                                            user.Photo = _azureBlobHelper.UploadFile("userphoto", photoFile, userAvatar);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                  
                        }

                        break;
                }
            }
            return socialNetworkToken;
        }

        private async Task<Claim> GetSocialNetworkToken(string providerName)
        {
            ClaimsIdentity claimsIdentity =
                await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);
            if (claimsIdentity != null)
            {
                return claimsIdentity.FindAll(providerName + "AccessToken").First();
            }
            return null;
        }
        
        public async Task AddUserDefaultChannelsToUser(User user)
        {
            var newChannel = Channel.CreateNewChannel(user.Id, user.Email);
            newChannel.Tag = _namesGenerator.GenerateChannelName(user.Email);
            //var newGroup = Channel.CreateNewGroup(user.Id, user.Email);
            //newGroup.Tag = _namesGenerator.GenerateGroupName(user.Email);
            _dbContext.Channels.Add(newChannel);
            //_dbContext.Channels.Add(newGroup);
            await _dbContext.SaveChangesAsync();
        }

        public async Task AddUserDefaultNotificationSettings(User user)
        {
            var principal = await user.GenerateUserIdentityAsync(UserManager);
            var defaultSettings = NotificationSetting.GetDefaultSet(new ClaimsPrincipal(principal));
            if (user.NotificationSettings != null && user.NotificationSettings.Any())
            {
                user.NotificationSettings.Clear();
                await _dbContext.SaveChangesAsync();
            }

            _dbContext.NotificationSettings.AddRange(defaultSettings);
            await _dbContext.SaveChangesAsync();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}