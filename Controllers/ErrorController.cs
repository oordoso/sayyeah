﻿using System.Web.Mvc;

namespace SayYeahTV.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error404()
        {
            ViewBag.ErrorNumber = 404;
            return View("CommonError");
        }

        public ActionResult Error500()
        {
            ViewBag.ErrorNumber = 500;
            return View("CommonError");
        }
    }
}