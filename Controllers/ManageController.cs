﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using SayYeahTV.DataAccess;
using SayYeahTV.DataAccess.Enums;
using SayYeahTV.DataAccess.Models;
using SayYeahTv.Dependencies.Services.Interfaces;
using SayYeahTV.Web.Helpers;
using SayYeahTV.Web.Models;
using SayYeahTV.Web.Models.Dto;
using SayYeahTV.Web.Models.OAuth;

namespace SayYeahTV.Web.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly SayYeahTVContext _dbContext;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IAzureBlobHelper _azureBlobHelper;
        private readonly IImageProcessor _imageProcessor;

        public ManageController(SayYeahTVContext dbContext, IAzureBlobHelper azureBlobHelper, IImageProcessor imageProcessor)
        {
            _dbContext = dbContext;
            _azureBlobHelper = azureBlobHelper;
            _imageProcessor = imageProcessor;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public ActionResult Index()
        {
            var user = _dbContext.Users.Find(User.Identity.GetUserId());
            var model = Mapper.Map<User, UserSettingsViewModel>(user);
            if (user.Birthday.HasValue)
            {
                model.Birthday = StringHelper.FromDate(user.Birthday.Value);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> SaveSettings(UserSettingsViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            var user = Mapper.Map<UserSettingsViewModel, User>(model);
            _dbContext.Users.Attach(user);
            _dbContext.Entry(user).Property(u => u.FirstName).IsModified = true;
            _dbContext.Entry(user).Property(u => u.LastName).IsModified = true;
            _dbContext.Entry(user).Property(u => u.Photo).IsModified = true;
            _dbContext.Entry(user).Property(u => u.AllowLocationDiscover).IsModified = true;
            _dbContext.Entry(user).Property(u => u.Gender).IsModified = true;
            _dbContext.Entry(user).Property(u => u.About).IsModified = true;
            _dbContext.Entry(user).Property(u => u.UserName).IsModified = true;
            _dbContext.Entry(user).Property(u => u.Birthday).IsModified = true;

            if (model.NewPhoto != null)
            {
                var extension = Path.GetExtension(model.NewPhoto.FileName);

                using (var photoStream = new MemoryStream())
                {
                    model.NewPhoto.InputStream.CopyTo(photoStream);
                    using (var userAvatar = _imageProcessor.ProcessUserAvatar(photoStream))
                    {
                        user.Photo = _azureBlobHelper.UploadFile("channellogo", string.Format("{0}{1}", Guid.NewGuid(), extension), userAvatar);
                    }
                }

                /*
                user.Photo = _azureBlobHelper.UploadFile("channellogo",
                    string.Format("{0}{1}", Guid.NewGuid(), extension), model.NewPhoto.InputStream);
                _dbContext.Entry(user).Property(u => u.Photo).IsModified = true;*/
            }

            if (model.Birthday == null)
            {
                user.Birthday = null;
            }
            else
            {
                DateTime dt;

                if (!StringHelper.ToDate(model.Birthday, out dt))
                    return Json(new {Errors = new[] {"Birhday is not recognized"}}, JsonRequestBehavior.AllowGet);

                user.Birthday = dt;
            }

            _dbContext.SaveChanges();

            var userId = User.Identity.GetUserId();
            var claims = await UserManager.GetClaimsAsync(userId);
            var genderClaims = claims.Where(c => c.Type == ClaimsNames.Gender);
            foreach (var genderClaim in genderClaims)
            {
                await UserManager.RemoveClaimAsync(userId, genderClaim);
            }
            await UserManager.AddClaimAsync(userId,
                    new Claim(ClaimsNames.Gender, model.Gender.ToString()));

            return Json(new { Success = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveNotificationSettings(SaveNotificationSettingsModel model)
        {
            var user = _dbContext.Users.Find(User.Identity.GetUserId());

            foreach (var setting in model.Notifications)
            {                
                var currentSetting = user.NotificationSettings.FirstOrDefault(
                    ns => ns.NotificationDeliveryMethod == NotificationDeliveryMethod.InSite && ns.NotificationEvent == setting.Key);
                if (currentSetting == null)
                {
                    user.NotificationSettings.Add(new NotificationSetting
                    {
                        NotificationDeliveryMethod = NotificationDeliveryMethod.InSite,
                        Enabled = setting.Value.InSite,
                        NotificationEvent = setting.Key
                    });
                }
                else
                {
                    currentSetting.Enabled = setting.Value.InSite;
                }

                currentSetting = user.NotificationSettings.FirstOrDefault(
                    ns => ns.NotificationDeliveryMethod == NotificationDeliveryMethod.Email && ns.NotificationEvent == setting.Key);
                if (currentSetting == null)
                {
                    user.NotificationSettings.Add(new NotificationSetting
                    {
                        NotificationDeliveryMethod = NotificationDeliveryMethod.Email,
                        Enabled = setting.Value.Email,
                        NotificationEvent = setting.Key
                    });
                }
                else
                {
                    currentSetting.Enabled = setting.Value.Email;
                }

                currentSetting = user.NotificationSettings.FirstOrDefault(ns => ns.NotificationDeliveryMethod == NotificationDeliveryMethod.Sms && ns.NotificationEvent == setting.Key);
                if (currentSetting == null)
                {
                    user.NotificationSettings.Add(new NotificationSetting
                    {
                        NotificationDeliveryMethod = NotificationDeliveryMethod.Sms,
                        Enabled = setting.Value.Sms,
                        NotificationEvent = setting.Key                        
                    });
                }
                else
                {
                    currentSetting.Enabled = setting.Value.Sms;
                }
            }

            await _dbContext.SaveChangesAsync();

            return Json(new {});
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Index", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            if (phoneNumber == null)
            {
                throw new HttpException(404, "Phone not found");
            }
                                
            return View(new VerifyPhoneNumberViewModel {PhoneNumber = phoneNumber});
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // GET: /Manage/RemovePhoneNumber
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return PartialView("_ChangePasswordPartial");
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                AddErrors(result);
                return Json(new { Errors = ModelState.Errors() }, JsonRequestBehavior.AllowGet);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                throw new HttpException(404, "User not found");
            }

            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return PartialView("_ManageLoginsPartial", new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        public async Task<ActionResult> WebShows()
        {
            var model = UserDto.GetShows(User.Identity.GetUserId());

            return PartialView("_WebShowsPartial", model);
        }

        public async Task<ActionResult> Upcomings()
        {
            var model = UserDto.GetUpcomings(User.Identity.GetUserId(), User.Identity.GetUserId());
            return PartialView("_UpcomingsPartial", model);
        }

        public async Task<ActionResult> Notifications()
        {
            var user = _dbContext.Users.Find(User.Identity.GetUserId());
            var userNotifications = user.NotificationSettings.ToList();
            var model = new Dictionary<NotificationEventType, NotificationTypes>();

            foreach (var type in ((NotificationEventType[])Enum.GetValues(typeof(NotificationEventType))))
            {
                NotificationEventType tmp = type;
                var notificationsTypes = userNotifications.Where(n => n.NotificationEvent == tmp).ToList();

                model.Add(tmp, new NotificationTypes
                {
                    Email = notificationsTypes.Any(nt => nt.NotificationDeliveryMethod == NotificationDeliveryMethod.Email && nt.Enabled),
                    Sms = notificationsTypes.Any(nt => nt.NotificationDeliveryMethod == NotificationDeliveryMethod.Sms && nt.Enabled),
                    InSite = notificationsTypes.Any(nt => nt.NotificationDeliveryMethod == NotificationDeliveryMethod.InSite && nt.Enabled)
                });
            }

            return PartialView("_NotificationsPartial", model);
        }
        

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var userId = User.Identity.GetUserId();
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, userId);
            if (loginInfo == null)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            var user = await UserManager.FindByIdAsync(userId);
            await UpdateUserInformation(loginInfo, user);
            await UserManager.UpdateAsync(user);

            var result = await UserManager.AddLoginAsync(userId, loginInfo.Login);
            return result.Succeeded ? RedirectToAction("Index") : RedirectToAction("Index", new { Message = ManageMessageId.Error });
        }

        private async Task<Claim> UpdateUserInformation(ExternalLoginInfo loginInfo, User user)
        {
            Claim socialNetworkToken = await GetSocialNetworkToken(loginInfo.Login.LoginProvider);
            string photoFile = string.Format("{0}.jpg", Guid.NewGuid().ToString("N"));

            if (socialNetworkToken != null)
            {
                GenderType genderType;
                switch (loginInfo.Login.LoginProvider)
                {
                    case "Google":
                        using (var webClient = new WebClient())
                        {
                            var jsonResponse =
                                webClient.DownloadString(
                                    new Uri(
                                        string.Format(
                                            "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={0}",
                                            socialNetworkToken.Value)));
                            var googlePlusInfo = JsonConvert.DeserializeObject<GooglePlusUserInfo>(jsonResponse);

                            if (string.IsNullOrEmpty(user.FirstName))
                            {
                                user.FirstName = googlePlusInfo.GivenName;
                            }

                            if (string.IsNullOrEmpty(user.LastName))
                            {
                                user.LastName = googlePlusInfo.FamilyName;
                            }
                            if (user.Gender == GenderType.SayGender)
                            {
                                if (Enum.TryParse(googlePlusInfo.Gender, true, out genderType))
                                {
                                    user.Gender = genderType;
                                }
                            }

                            if (string.IsNullOrEmpty(user.Photo))
                            {
                                using (var photoStream = new MemoryStream(webClient.DownloadData(googlePlusInfo.Picture)))
                                {
                                    using (var userAvatar = _imageProcessor.ProcessUserAvatar(photoStream))
                                    {
                                        user.Photo = _azureBlobHelper.UploadFile("userphoto", photoFile, userAvatar);
                                    }
                                }
                            }
                        }
                        break;

                    case "Facebook":
                        var facebookClient = new FacebookClient(socialNetworkToken.Value);
                        dynamic info = facebookClient.Get("/me");
                        if (string.IsNullOrEmpty(user.FirstName))
                        {
                            user.FirstName = info.first_name;
                        }

                        if (string.IsNullOrEmpty(user.LastName))
                        {
                            user.LastName = info.last_name;
                        }

                        if (user.Gender == GenderType.SayGender)
                        {
                            if (Enum.TryParse(info.gender, true, out genderType))
                            {
                                user.Gender = genderType;
                            }
                        }

                        if (string.IsNullOrEmpty(user.Photo))
                        {
                            using (WebClient webClient = new WebClient())
                            {
                                using (
                                    var photoStream =
                                        new MemoryStream(
                                            webClient.DownloadData(string.Format("https://graph.facebook.com/{0}/picture?type=large", info.id))))
                                {
                                    using (var userAvatar = _imageProcessor.ProcessUserAvatar(photoStream))
                                    {
                                        user.Photo = _azureBlobHelper.UploadFile("userphoto", photoFile, userAvatar);
                                    }
                                }
                            }
                        }

                        break;
                }
            }
            return socialNetworkToken;
        }

        private async Task<Claim> GetSocialNetworkToken(string providerName)
        {
            ClaimsIdentity claimsIdentity =
                await AuthenticationManager.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);
            if (claimsIdentity != null)
            {
                return claimsIdentity.FindAll(providerName + "AccessToken").First();
            }
            return null;
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}