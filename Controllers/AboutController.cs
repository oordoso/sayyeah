﻿using System.Web.Mvc;

namespace SayYeahTV.Web.Controllers
{
    public class AboutController : Controller
    {
        public ActionResult Company()
        {
            return View();
        }

        public ActionResult Team()
        {
            return View();
        }

        public ActionResult Jobs()
        {
            return View();
        }

        public ActionResult Directors()
        {
            return View();
        }

        public ActionResult Investors()
        {
            return View();
        }

        public ActionResult Press()
        {
            return View();
        }

        public ActionResult Advertising()
        {
            return View();
        }

        public ActionResult BrandedContent()
        {
            return View();
        }

        public ActionResult Partnership()
        {
            return View();
        }

        public ActionResult Policy()
        {
            return View();
        }

        public ActionResult Tos()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}